--创建一个商品库
CREATE DATABASE
IF NOT EXISTS eshop DEFAULT CHARACTER
SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;

use  eshop
--建立一个商品表
CREATE TABLE `item`(
		`item_id` int (11) UNSIGNED NOT NULL auto_increment,
		 `item_name` varchar(100) not null COMMENT '商品名称',
		 `item_price` DECIMAL(10,2) not null COMMENT '商品价格',
			PRIMARY key (`item_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='商品表';
--建立一个购物车表
CREATE TABLE `cart` (
		`cart_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`user_id` int(11) unsigned NOT NULL COMMENT '用户id',
		`item_id` int(11) unsigned NOT NULL COMMENT '商品id',
		`item_num` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
		`add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
		PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--加入一条数据
INSERT INTO item (item_name, item_price)
VALUES
	(
		'华为全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK ',
		1559
	);


--加入多条数据
INSERT INTO item (item_name, item_price)
VALUES
	(
		'小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK',
		1349
	),
	(
		'TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',
		2199
	),
	(
		'海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视',
		1349
	),
	(
		'海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能',
		2699
	),
  ('长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）',
   2099),
	(
		'长TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',
		1999
	);

--加入一条购物车信息

INSERT INTO cart (
	user_id,
	item_id,
	item_num,
	add_time
)
VALUES
	(
		1,
		1,
		1,
		unix_timestamp(now())
	);

--加入多条购物车信息


INSERT INTO cart (user_id, item_id, item_num)
VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 4, 1),
	(3, 5, 1),
	(4, 1, 2);

--查询
--1. 查询商品表所有数据。
select * from item;
--2. 查询价格小于2000的商品。
select * from item where itme_price < 2000;
--3. 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';
--4. 查询商品表，假设每页3条记录，查询第1页的数据。
select * from  item limit 3;
--5. 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 OFFSET 3;
6. 查询所有商品，按照价格进行升序排序。
select * from item ORDER BY item_price DESC;
7. 查询价格小于2000的商品总数。
select count(*) from item where itme_price <2000;
8. 查询购物车中所有数据。
select * from cart;
9. 查询每个用户加入购物车的商品总数。
select user_id,sum(*) from cart group by user_id; 
10. 查询加入购物车的商品总数小于2的用户。
select user_id,sum(*) as 商品总数 from cart group by user_id having 商品总数 < 2;  
11. 查询有加入购物车的商品。
select distinct item.* from cart inner join item on cart.item_id=item.item_id;
12. 查询没有加入购物车的商品。
select item.* from item left join cart on item.item_id=cart.item_id where cart.cart_id is null;

--删除
1. 用户4将将商品1从购物车中移除。

DELETE
FROM
	cart
WHERE
	user_id = 4
AND item_id = 1;
2. 用户3清空购物车。
DELETE FROM cart WHERE user_id=3 ;
3. 移除购物车中的所有数据：
TRUNCATE TABLE cart;
	