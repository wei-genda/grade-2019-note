--建库bolg
CREATE DATABASE IF NOT EXISTS blog
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;
--建category
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='分类表';
--建article
CREATE TABLE `article` (
`article_id`  int(255) NOT NULL COMMENT '文章id' ,
`article_title`  varchar(100) NOT NULL COMMENT '文章标题' ,
`category_id`  int(11) NOT NULL COMMENT '分类Id' ,
`article_author`  varchar(20) NULL DEFAULT '' COMMENT '文章作者' ,
`article_content`  varchar(255) NOT NULL COMMENT '文章内容' ,
`update_time`  int(11) NOT NULL COMMENT '修改时间' ,
`add_time`  int(11) NOT NULL COMMENT '发布时间' ,
PRIMARY KEY (`article_id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='文章表';
;