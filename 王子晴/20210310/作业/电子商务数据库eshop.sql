-- 一、创建库
-- 1.创建电子商务数据库，eshop：
create database if not exists eshop
		DEFAULT CHARACTER SET utf8mb4
		DEFAULT COLLATE utf8mb4_general_ci;	
use eshop;
-- 2.创建商品表item：
CREATE TABLE `item` (
  `item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL COMMENT '商品名称',
  `item_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品表';
-- 3.创建购物车表cart：
CREATE TABLE `cart` (
  `cart_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `item_id` int(11) unsigned NOT NULL COMMENT '商品id',
  `item_num` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 二、插入数据
-- 1.商品表item
insert into item (item_name,item_price) values ('小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK','1349'),
																							 ('TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机	','2199'),
																							 ('海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视','1349'),
																							 ('海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能','2699'),
																							 ('TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机','1999'),
																							 ('长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）','2099');
-- 2.购物车表cart
insert into cart (user_id,item_id,item_num) values (1,1,1),(2,2,2),(3,4,1),(3,5,1),(4,1,2);

-- 三、查询数据
-- 1. 查询商品表所有数据。
select * from item;
-- 2. 查询价格小于2000的商品。
select * from item where item_price < 2000;
-- 3. 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';
-- 4. 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3 ;
-- 5. 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 offset 3 ;
-- 6. 查询所有商品，按照价格进行升序排序。
select * from item order by item_price asc;
-- 7. 查询价格小于2000的商品总数。
select count(*) as 商品总数 from item where item_price < 2000;
-- 8. 查询购物车中所有数据。
select * from cart;
-- 9. 查询每个用户加入购物车的商品总数。
select user_id,sum(item_num) as 商品总数 from cart group by user_id;
-- 10. 查询加入购物车的商品总数小于2的用户。
select cart.user_id,sum(item_num) as 商品总数 from cart inner join (select user_id,sum(item_num) as 商品总数 from cart group by user_id) as num on cart.user_id = num.user_id where num.`商品总数` < 2;	-- 法一
select user_id,sum(item_num) as 商品总数 from cart group by user_id having sum(item_num) < 2 ;		-- 法二
-- 11. 查询有加入购物车的商品。
select * from item where item_id in (select item_id from cart );		-- 法一（子查询，mysql中不推荐，性能低）
select distinct item. * from item inner join cart on item.item_id = cart.item_id ;	-- 法二（连接查询）
-- 12. 查询没有加入购物车的商品。
select * from item where item_id not in (select item_id from cart);	-- 法一（子查询）
select * from item left join cart on item.item_id = cart.item_id where cart.item_id is null ;	-- 法二（连接查询）
-- 四、删除数据
-- 1. 用户4将将商品1从购物车中移除。
delete from cart where user_id = 4 and item_id = 1 ;
-- 2. 用户3清空购物车。
delete from cart where user_id = 3 ;
-- 3. 移除购物车中的所有数据：
--     1. 使用delete进行删除
			 delete from cart ;	
--     2. 使用truncate进行删除
			 truncate table cart ;
--   	 3. 对比delete和truncate的差别
-- 答：（1）delete自增不会重置，而truncate会，从1继续开始
-- 		 （2）灵活性：delete可以条件删除数据，而truncate只能删除表所有数据
-- 		 （3）效率：delete效率低于truncate。delete是一行一行地删除，而truncate会重建表结构
