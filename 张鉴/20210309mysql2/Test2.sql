CREATE DATABASE Test2;
use Test2;
-- 创建employee表
CREATE TABLE employee (
	id INT(11)   UNSIGNED NOT NULL auto_increment COMMENT '员工编号',
	name VARCHAR(25)  NOT NULL COMMENT '员工姓名',
	deptId  INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门编号',
	salary FLOAT(11,2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '员工薪资',
  update_time INT(11) UNSIGNED NOT NULL COMMENT '修改时间',
  add_time INT(11) UNSIGNED NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`id`)
)ENGINE=innodb DEFAULT CHARSET=utf8mb4 COMMENT '员工表';



-- 创建department表
CREATE TABLE department(
deptId INT(11)  UNSIGNED NOT NULL auto_increment COMMENT '部门编号',
name VARCHAR(25)  NOT NULL COMMENT '部门名称',
level INT(11)  UNSIGNED NOT NULL DEFAULT 0  COMMENT '部门等级',
parentDeptId INT(11)  UNSIGNED NOT NULL  DEFAULT 0 COMMENT '上级部门编号',
deptLeader INT(11)  UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门编号',
update_time INT(11)  UNSIGNED NOT NULL COMMENT '修改时间',
add_time INT(11)  UNSIGNED NOT NULL COMMENT '增加时间',
PRIMARY KEY (`deptId`)
)ENGINE=innodb DEFAULT CHARSET=utf8mb4 COMMENT '部门表';
-- 修改 employee 表名为 employee_info
ALTER TABLE employee RENAME TO employee_info;
-- 修改 department 表名为 department_info
ALTER TABLE department RENAME TO department_info;
-- 添加字段
ALTER TABLE employee_info ADD  sex tinyint(3) UNSIGNED DEFAULT 3 COMMENT '员工性别：1男2女3保密';
ALTER TABLE employee_info ADD  address VARCHAR(100) NOT NULL DEFAULT ' ' COMMENT '住址';
ALTER TABLE employee_info ADD join_time INT(11) NOT NULL COMMENT '入职时间';
-- 员工信息表工资salary字段进行修改
ALTER TABLE employee_info CHANGE salary salary 	decimal(11,2) COMMENT '工资';