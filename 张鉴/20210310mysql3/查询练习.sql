-- 查询商品表所有数据。
select * from item;
-- 查询价格小于2000的商品。
select * from item where item_price < 2000 ;
-- 查询名称中包含 海信 的商品。
select * from item where item_name LIKE '%海信%';
-- 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item LIMIT 3 ;
-- 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item LIMIT 3 OFFSET 3 ;
-- 查询所有商品，按照价格进行升序排序。
select * from item order by item_price asc;
-- 查询价格小于2000的商品总数。
SELECT COUNT(*) as 商品总数 from item where item_price <2000;
-- 查询购物车中所有数据。
SELECT * from cart;
-- 查询每个用户加入购物车的商品总数。
SELECT user_id,sum(item_num) from cart GROUP BY user_id;
-- 查询加入购物车的商品总数小于2的用户。
SELECT user_id from cart GROUP BY user_id HAVING SUM(user_id)<2;
-- 查询有加入购物车的商品。

SELECT DISTINCT item.* FROM item INNER JOIN cart on item.item_id = cart.item_id;
-- 查询没有加入购物车的商品。
SELECT
    item.*
FROM
    item
LEFT JOIN cart ON item.item_id = cart.item_id
WHERE
    ISNULL(cart.cart_id);