-- 用户4将将商品1从购物车中移除。
DELETE from cart where user_id = 4 AND item_id = 1;
-- 用户3清空购物车。
DELETE FROM cart where user_id = 3;
-- 移除购物车中的所有数据：
   -- 使用delete进行删除
  DELETE * from cart;
   -- 使用truncate进行删除
  TRUNCATE * from cart;
   -- 对比delete和truncate的差别
      -- 自增发送改变 DELETE 改变了 TRUNCATE 没有改变
 