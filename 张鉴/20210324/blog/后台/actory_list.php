<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 11:02
 */
date_default_timezone_set("PRC");
$sdn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($sdn,"root","123456");
$sql = "select * from article order by article_id asc";
$db->exec("set names utf8");
$result = $db->query($sql);
$statement = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link  href="css/top.css" rel="stylesheet" type="text/css"/>
    <link  href="css/base.css" rel="stylesheet" type="text/css"/>
    <link  href="css/left.css" rel="stylesheet" type="text/css"/>
    <link   href="css/right.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div id="all">
    <div id="top">
        <!--        <li id="a">-->
        <!--            欢迎你：admin 退出登录-->
        <!--        </li>-->
        <div id="top1">
            <h1>博客管理系统</h1>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="actory_list.php">新闻管理</a></li>
            <!--            <li><a href="#">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <li><a href="#">首页</a>>&nbsp;<a href="actory_list.php">文章管理</a>>&nbsp;<a href="#">文章列表</a></li>
        <li><p><a href="actory_add.php">增加文章</a></p></li>
        <!--        <li ><input  type="button" value="全选" class="f"/><span><a href="#">删除选中任务</a></span></li>-->
        <form>
            <table style="width:100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td></td>
                    <td>文章ID</td>
                    <td>文章标题</td>
                    <td>文章分类</td>
                    <td>文章简介</td>
                    <td>添加时间</td>
                    <td>修改时间</td>
                    <td>操作</td>
                </tr>
                <?php foreach ($statement as $item):?>
                <tr>
                    <td><input type="checkbox"/></td>
                    <td><?php echo $item['article_id']?></td>
                    <td><?php echo $item['article_title']?></td>
                    <td><?php
                        $sdn = "mysql:host=127.0.0.1;dbname=blog";
                        $db = new PDO($sdn,"root","123456");
                        $sql = "select * from category where category_id ='{$item['category_id']}' ";
//                        $db->exec("set names utf8");
                        $result=$db->query($sql);
                        $statement=$result->fetch(PDO::FETCH_ASSOC);
                        echo $statement['category_name'];
                        ?></td>
                    <td><?php echo $item['content']?></td>
                       <td><?php echo date("y-m-d h:i:s",$item['add_time'])?></td>
                       <td><?php echo date("y-m-d h:i:s",$item['update_time'])?></td>
                    <td>
                        <a href="actory_idet.php?article_id=<?php echo $item['article_id']?>">编辑</a>
                        <a href="actory_delete.php?article_id=<?php echo $item['article_id']?>">删除</a>
                    </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </form>
    </div>
</div>
</body>
</html>
