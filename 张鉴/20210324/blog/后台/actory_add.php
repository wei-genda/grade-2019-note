<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 11:01
 */
date_default_timezone_set("PRC");
$sdn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($sdn,"root","123456");
$sql = "select * from category order by category_id asc";
$db->exec("set names utf8");
$result = $db->query($sql);
$statement = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link  href="css/base.css" rel="stylesheet" type="text/css"/>
    <link  href="css/top.css" rel="stylesheet" type="text/css"/>
    <link href="css/left.css"  rel="stylesheet" type="text/css"/>
    <link  href="css/right2.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div id="all">
    <div id="top">
<!--        <li id="a">-->
<!--            欢迎你：admin 退出登录-->
<!--        </li>-->
        <div id="top1">
            <h1>博客管理系统</h1>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">新闻管理</a></li>
<!--            <li><a href="#">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <li><a href="#">首页</a>>&nbsp;<a href="actory_list.php">文章管理</a>>&nbsp;<a href="#">添加文章</a></li>
        <form action="actory_add_save.php" method="post">
            <table cellpadding="0" cellspacing="0" style="width:100%">
                <tr>
                    <td class="b" >文章名称</td>
                    <td><input type="text" name="actory_name"/></td>
                </tr>
                <tr>
                    <td class="b" >文章分类</td>
                    <td>
                        <select name="category_id">
                            <?php foreach ($statement as $item):?>
                            <option value="<?php echo $item['category_id']?>">
                                <?php echo $item['category_name'] ?>
                            </option>
                            <?php endforeach;?>
                        </select>

                    </td>
                </tr>
                <tr>
                    <td >文章简介</td>
                    <td><textarea cols="60" rows="15" name="content"></textarea></td>
                </tr>
                <tr>
                    <td >文章内容</td>
                    <td><textarea cols="60" rows="15" name="intro"></textarea></td>
                </tr>
                <tr>
                    <td class="b" ></td>
                    <td><input type="submit" value="提交"/>&nbsp;&nbsp;<input type="reset" value="重置"/></td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>

