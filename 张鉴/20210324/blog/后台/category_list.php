<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 11:03
 */
date_default_timezone_set("PRC");
$sdn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($sdn,"root","123456");
$sql = "select * from category order by category_id asc";
$db->exec("set names utf8");
$result = $db->query($sql);
$statement = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link  href="css/top.css" rel="stylesheet" type="text/css"/>
    <link  href="css/base.css" rel="stylesheet" type="text/css"/>
    <link  href="css/left.css" rel="stylesheet" type="text/css"/>
    <link   href="css/right.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div id="all">
    <div id="top">
<!--        <li id="a">-->
<!--            欢迎你：admin 退出登录-->
<!--        </li>-->
        <div id="top1">
            <h1>博客管理系统</h1>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="actory_list.php">新闻管理</a></li>
<!--            <li><a href="#">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <li><a href="#">首页</a>>&nbsp;<a href="category_list.php">分类管理</a>>&nbsp;<a href="#">分类列表</a></li>
        <li><p><a href="category_add.php">增加页面</a></p></li>
<!--        <li ><input  type="button" value="全选" class="f"/><span><a href="#">删除选中任务</a></span></li>-->
        <form>
            <table style="width:100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td></td>
                    <td>分类ID</td>
                    <td>分类名称</td>
                    <td>增加时间</td>
                    <td>修改时间</td>
                    <td>操作</td>
                </tr>
                <?php foreach ($statement as $item):?>
                <tr>
                    <td><input type="checkbox"/></td>
                    <td><?php echo $item['category_id']?></td>
                    <td><?php echo $item['category_name']?></td>
                    <td><?php echo date("y-m-d H:i:s",$item['add_time'])?></td>
                    <td><?php echo date("y-m-d H:i:s",$item['update_time'])?></td>
                    <td>
                        <a href="category_idet.php?category_id=<?php echo $item['category_id']?>">编辑</a>
                        <a href="category_declete.php?category_id=<?php echo $item['category_id']?>">删除</a>
                    </td>

                </tr>
                <?php endforeach;?>
            </table>
        </form>
    </div>
</div>
</body>
</html>

