<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 11:03
 */?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link  href="css/top.css" rel="stylesheet" type="text/css"/>
    <link  href="css/base.css" rel="stylesheet" type="text/css"/>
    <link  href="css/left.css" rel="stylesheet" type="text/css"/>
    <link   href="css/right.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div id="all">
    <div id="top">
        <li id="a">
            欢迎你：admin 退出登录
        </li>
        <div id="top1">
            <h1>博客管理系统</h1>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">新闻管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <li><a href="#">首页</a>>&nbsp;<a href="#">新闻管理</a>>&nbsp;<a href="#">新闻列表</a></li>
        <li><p>增加页面</p></li>
        <li ><input  type="button" value="全选" class="f"/><span><a href="#">删除选中任务</a></span></li>
        <form>
            <table style="width:100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td></td>
                    <td>ID</td>
                    <td>标题</td>
                    <td>分类</td>
                    <td>简介</td>
                    <td>发布时间</td>
                    <td>修改时间</td>
                    <td>操作</td>
                </tr>
                <tr>
                    <td><input type="checkbox"/></td>
                    <td>1</td>
                    <td>vue入门知识</td>
                    <td>wed前端</td>
                    <td>vue入门指南</td>
                    <td>2021-03-18 10：26</td>
                    <td>2021-03-18 10：28</td>
                    <td><a href="#">编辑</a></td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>

