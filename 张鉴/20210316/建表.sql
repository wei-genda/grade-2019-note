CREATE TABLE category(
 category_id INT UNSIGNED NOT NULL auto_increment,
 category_name VARCHAR(20) NOT NULL COMMENT '分类名称',
 category_desc VARCHAR(200) NOT NULL COMMENT '分类描述',
 add_time int(11) NOT NULL DEFAULT 0 COMMENT '增加时间',
 update_time int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
 PRIMARY KEY (`category_id`)
)ENGINE = InnoDB  CHARSET = utf8 COMMENT '分类表'
ALTER TABLE category CHANGE category_id category_id INT(11)
CREATE TABLE article(
  article_id INT(10) UNSIGNED NOT NULL auto_increment,
  category_id INT(11) NOT NULL,
  article_title VARCHAR(45) NOT NULL COMMENT '文章标题',
  content LONGTEXT NOT NULL COMMENT '文章内容',
  add_time int(11) NOT NULL DEFAULT 0 COMMENT '增加时间',
  update_time int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY(`article_id`)
)ENGINE = InnoDB  CHARSET = utf8 COMMENT '文章表'