1.  完成 php高级_01_mysql/10_mysql操作表数据.md 课堂练习

1. 查询商品表所有数据。
select * from item;
2. 查询价格小于2000的商品。
select * from item where itrm_price < 2000;
3. 查询名称中包含 海信 的商品。
select* from item where item_name like '%海信%'；
4. 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3;
5. 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 offset 3;
6. 查询所有商品，按照价格进行升序排序。
select * from item order by item_price asc;
7. 查询价格小于2000的商品总数。

8. 查询购物车中所有数据。
select * from cart;
9. 查询每个用户加入购物车的商品总数。
select user_id, sum(item_num) from cart group by user_id;
10. 查询加入购物车的商品总数小于2的用户。
select user_id from cart group by user_id having sum(item_num) < 2;
11. 查询有加入购物车的商品。
select distinct item.* from item inner join cart on item.item_id = cart.item_id;
12. 查询没有加入购物车的商品。
select item.* from item left join cart on item.item_id = cart.item_id where cart.cart_id is null;
select * from item where item_id not in (select item_id from cart);

1. 用户4将将商品1从购物车中移除。
delete from cart where user_id=4 and item_id=1;
2. 用户3清空购物车。
truncate table item where user_id=3;
3. 移除购物车中的所有数据：
	1. 使用delete进行删除
delete from cart；
	2. 使用truncate进行删除
truncate table item；
	3. 对比delete和truncate的差别
delete删除部分数据，truncate在需要保留该表但删除记录的时候使用