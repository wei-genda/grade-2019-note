<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/23
 * Time: 10:19
 */
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>分类编辑</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div class="container">
    <div class="header">
        <h2>博客管理系统</h2>
        <div id="login_out">欢迎您：admin<a href="#">&nbsp;退出登录</a></div>
    </div>
    <div class="left">
        <ul>
            <li><a href="category.php">分类管理</a></li>
            <li><a href="article.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div class="right">
        <div class="head">
            <a href="#">首页</a>&gt;
            <a href="category.php">分类管理</a>&gt;
            <a href="category_add.php">分类增加</a>&gt;
        </div>
        <div class="table">
            <form action="category_edit_save.php" method="post">
                <table border="" cellpadding="" cellspacing="0">
                    <tr>
                        <td id="a">分类名称:</td>
                        <td id="b"><input id="c" type="text" value="" name="category_name" /></td>
                    </tr>
                    <tr>
                        <td id="a">分类描述:</td>
                        <td id="b"><textarea id="d" rows="15" cols="60" name="category_desc"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="b">
                            <input type="submit" value="提交" id="save" />
                            <input type="reset" value="取消" id="reset" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
