<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM article ORDER BY article_id DESC";
$result = $db->query($sql);
$articleList = $result->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台文章首页</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div class="container">
    <div class="header">
        <h2>博客管理系统</h2>
        <div id="login_out">欢迎您：admin<a href="#">&nbsp;退出登录</a></div>
    </div>
    <div class="left">
        <ul>
            <li><a href="category.php">分类管理</a></li>
            <li><a href="article.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div class="right">
        <div class="head">
            <a href="#">首页</a>&gt;
            <a href="article.php">文章管理</a>&gt;
            <a href="article.php">文章列表</a>&gt;
        </div>
        <div class="center">
            <input type="button" value="全选" id="all" />
            <a href="#">删除选中任务</a>
            <a href="article_add.php" id="new_add">增加文章</a>
        </div>
        <div class="table">
            <table border="" cellpadding="" cellspacing="0">
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>标题</th>
                    <th>分类</th>
                    <th>简介</th>
                    <th>发表时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($articleList as $item): ?>
                <tr>
                    <td><input type="checkbox" name="article_id[]" value="<?php echo $item['article_id']; ?>" /></td>
                    <td><?php echo $item['article_id']; ?></td>
                    <td><?php echo $item['article_title']; ?></td>
                    <td><?php echo $item['category_id']; ?></td>
                    <td><?php echo $item['content']; ?></td>
                    <td><?php echo $item['update_time']; ?></td>
                    <td><?php echo $item['add_time']; ?></td>
                    <td>
                        <a href="article_edit.php?id=<?php echo $item['article_id']; ?>">编辑</a>
                        <a href="article_delete.php?id=<?php echo $item['article_id']; ?>">删除</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
</body>
</html>
