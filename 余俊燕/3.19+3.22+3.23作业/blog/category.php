<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM category ORDER BY category_id DESC";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台分类首页</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script src="js/jquery.js"></script>
</head>
<body>
<div class="container">
    <div class="header">
        <h2>博客管理系统</h2>
        <div id="login_out">欢迎您：admin<a href="#">&nbsp;退出登录</a></div>
    </div>
    <div class="left">
        <ul>
            <li><a href="category.php">分类管理</a></li>
            <li><a href="article.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div class="right">
        <div class="head">
            <a href="#">首页</a>&gt;
            <a href="category.php">分类管理</a>&gt;
            <a href="category.php">分类列表</a>&gt;
        </div>
        <div class="center">
            <input type="button" value="全选" id="all" />
            <a href="javascript:void(0);">删除选中任务</a>
            <a href="category_add.php" id="new_add">增加分类</a>
        </div>
        <form id="list-form" action="#" method="post">
            <table border="" cellpadding="" cellspacing="0">
                    <tbody>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>名称</th>
                        <th>分类描述</th>
                        <th>增加时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $item): ?>
                        <tr>
                            <td><input type="checkbox" name="category_id[]" value="<?php echo $item['category_id']; ?>" /></td>
                            <td><?php echo $item['category_id']; ?></td>
                            <td><?php echo $item['category_name']; ?></td>
                            <td><?php echo $item['category_desc']; ?></td>
                            <td><?php echo $item['update_time']; ?></td>
                            <td><?php echo $item['add_time']; ?></td>
                            <td>
                                <a class="category_edit" href="category_edit.php?id=<?php echo $item['category_id']; ?>">编辑</a>
                                <a class="category_delete" href="category_delete.php?id=<?php echo $item['category_id']; ?>">删除</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
            </table>
        </form>
    </div>
</div>
</body>
</html>

