<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM category";
$result = $db->query($sql);
$categoryAdd = $result->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>文章增加</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div class="container">
    <div class="header">
        <h2>博客管理系统</h2>
        <div id="login_out">欢迎您：admin<a href="#">&nbsp;退出登录</a></div>
    </div>
    <div class="left">
        <ul>
            <li><a href="category.php">分类管理</a></li>
            <li><a href=article.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div class="right">
        <div class="head">
            <a href="#">首页</a>&gt;
            <a href="article.php">文章管理</a>&gt;
            <a href="article_add.php">增加文章</a>&gt;
        </div>
        <div class="table">
            <form method="post" action="article_add_save.php">
                <table border="" cellpadding="" cellspacing="0">
                    <tr>
                        <td id="a">文章id：</td>
                        <td id="b"><input id="c" type="text" value="" name="article_id" /></td>
                    </tr>
                    <tr>
                        <td id="a">文章标题：</td>
                        <td id="b"><input id="c" type="text" value="" name="article_title" /></td>
                    </tr>
                    <tr>
                        <td id="a">文章分类：</td>
                        <td id="b">
                            <select id="e" name="category_id">
                                <?php foreach($categoryAdd as $item): ?>
                                <option value="<?php echo $item['category_id']; ?>"><?php echo $item['category_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td id="a">文章内容:</td>
                        <td id="b"><textarea id="d" rows="15" cols="60" name="content"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td id="b">
                            <input type="submit" value="提交" id="save" />
                            <input type="button" value="重置" id="reset" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
