CREATE DATABASE IF NOT EXISTS eshop
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;	

CREATE TABLE item(
	item_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	item_name VARCHAR(100) NOT NULL COMMENT '商品名称',
	item_price DECIMAL(10,2) NOT NULL COMMENT '商品价格',
	PRIMARY KEY (item_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='商品表';

CREATE TABLE cart(
	cart_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	user_id INT(11) UNSIGNED NOT NULL COMMENT '用户id',
	item_id INT(11) UNSIGNED NOT NULL COMMENT '商品id',
	item_num INT(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT '商品数量',
	add_time INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '加入时间',
	PRIMARY KEY (cart_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='购物车表';


INSERT INTO item 
	(item_name , item_price) 
	VALUES
	('小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK',1349),
	('TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机', 2199),
	('海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视', 1349),
	('海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能', 2699),
	('TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机', 1999),
	('长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）', 2099);


INSERT INTO cart
	(user_id , item_id , item_num)
	VALUES
	(1,1,1),
	(2,2,2),
	(3,4,1),
	(3,5,1),
	(4,1,2);


-- 查询商品表所有数据。
SELECT * FROM item;
-- 查询价格小于2000的商品。
SELECT * FROM item WHERE item_price < 2000;
-- 查询名称中包含 海信 的商品。
SELECT * FROM item WHERE item_name LIKE '%海信%';
-- 查询商品表，假设每页3条记录，查询第1页的数据。
SELECT * FROM item LIMIT 3;
-- 查询商品表，假设每页3条记录，查询第2页的数据。
SELECT * FROM item LIMIT 3 OFFSET 3;
-- 查询所有商品，按照价格进行升序排序。
SELECT * FROM item ORDER BY item_price ASC;
-- 查询价格小于2000的商品总数。
SELECT COUNT(*) FROM item WHERE item_price < 2000;
-- 查询购物车中所有数据。
SELECT * from cart;
-- 查询每个用户加入购物车的商品总数。
SELECT user_id , SUM(item_num) FROM cart GROUP BY user_id;
-- 查询加入购物车的商品总数小于2的用户。
SELECT user_id from cart GROUP BY user_id HAVING SUM(item_num) < 2;
-- 查询有加入购物车的商品。
SELECT DISTINCT item.* FROM item INNER JOIN cart ON item.item_id = cart.item_id;
-- 查询没有加入购物车的商品。
SELECT item.* FROM item INNER JOIN cart ON item.item_id = cart.item_id WHERE ISNULL(cart.cart_id);



-- 1.用户4将商品1从购物车中移除。
DELETE FROM cart WHERE user_id = 4 AND item_id = 1;
-- 2.用户3清空购物车。
DELETE FROM cart WHERE user_id = 3;
-- 3.移除购物车中的所有数据：
-- i.使用delete进行删除
DELETE FROM cart;
-- ii.使用truncate进行删除
TRUNCATE TABLE cart;
-- iii.对比delete和truncate的差别
-- 执行delete语句后，表依然会包含空页;
-- truncate table删除表中的所有行，但表结构以及列、约束、索引等保持不变.