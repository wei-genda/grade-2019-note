-- 创建一个blog数据库
create database if not exists blog
DEFAULT CHARACTER set utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

-- 使用blog数据库
USE blog

-- 创建分类表  存储分类信息
create table category(
category_id int(11) UNSIGNED not null auto_increment,
category_name VARCHAR(20) not null comment '分类名称',
category_desc varchar(300) not null comment '分类描述',
update_time int(11) not null default 0 comment '修改时间',
add_time int(11) not null comment '增加时间',
primary key (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment='分类表';


-- 创建文章表
create table artcle(
artcle_id int(11)not null,
category_id int(11)not null comment '所属分类',
artcle_title varchar(30) not null comment '文章标题',
artcle_content varchar(1000) not null comment '文章内容',
update_time int(11) not null default 0 comment '跟新时间',
add_time int(11) not null comment '增加时间',
primary key(`artcle_id`)

)ENGINE=INNODB default charset=utf8mb4 comment='文章表';


