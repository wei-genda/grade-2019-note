--  05mysql创建表   
-- 1、创建员工信息表   表引擎为innodb 表字符集为utf8mb4
create table employee (
`id` int(11) UNSIGNED auto_increment not null comment '员工编号',
`name` varchar(25) not null comment '员工名称',
`deptId` int(11) UNSIGNED DEFAULT 0 comment '所在部门编号',
`salary` float(11,2) UNSIGNED not null DEFAULT 0 comment '工资',
`update_time` int(11) UNSIGNED not null comment '修改时间',
`add_time` int(11)UNSIGNED not null comment '增加时间',
PRIMARY key (`id`)
) engine=innodb default charset=utf8mb4 comment='员工信息表';


-- 2、创建部门信息表department 表引擎为innodb 表字符集为utf8mb4
create table department (
deptId int(11) UNSIGNED auto_increment not null comment '部门编号',
name varchar(25) not null comment '部门名称',
level int(11) UNSIGNED not null default 0 comment '部门等级',
parentDeptId int(11) UNSIGNED not null default 0 comment '上级部门编号',
deptLeader int(11) UNSIGNED not null default 0 comment '部门领导',
update_time int(11) UNSIGNED not null comment '修改时间',
add_time int(11)UNSIGNED not null comment '增加时间',
PRIMARY key (`deptId`)
) engine=innodb default charset=utf8mb4 comment='部门信息表';


-- 06MySQL修改表
-- 1、修改employee表名为employee_info
alter table employee rename to employee_info

-- 2、修改department表名为department_info
alter table department rename to department_info

-- 07MySQL添加字段
alter table employee_info add sex tinyint(1) UNSIGNED default 3 comment '员工性别';
alter table employee_info add address varchar(100) not null default ' ' comment '住址';
alter table employee_info add join_time int(11) not null comment '入职时间';

-- 08MySQL修改字段
alter table employee_info change float decimal(11,2);

-- 09MySQL删除字段
-- alter table <表名> drop <字段名>
alter table employee_info drop emptId;
alter table employee_info drop add_time








