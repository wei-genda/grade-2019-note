/*
Navicat MySQL Data Transfer

Source Server         : admin
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : oa

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-16 17:05:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `deptId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `name` varchar(25) NOT NULL COMMENT '部门名称',
  `level` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '部门等级',
  `parentDeptId` int(11) unsigned DEFAULT '0' COMMENT '上级部门编号',
  `deptLeader` int(11) unsigned DEFAULT '0' COMMENT '部门领导',
  `update_time` int(11) unsigned NOT NULL COMMENT '修改时间',
  `add_time` int(11) unsigned NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`deptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='部门信息表';

-- ----------------------------
-- Records of department
-- ----------------------------

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `name` varchar(25) NOT NULL COMMENT '员工名称',
  `deptId` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所在部门编号',
  `salary` float(11,2) unsigned NOT NULL COMMENT '薪资',
  `update_time` int(11) unsigned NOT NULL COMMENT '修改时间',
  `add_time` int(11) unsigned NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='员工信息表';

-- ----------------------------
-- Records of employee
-- ----------------------------
