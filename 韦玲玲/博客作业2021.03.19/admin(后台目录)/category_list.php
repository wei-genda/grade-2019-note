<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1; dbname=blog";
$db =new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$sql = "select * from category order by `article_id` desc ";
$result = $db->query($sql);
//var_dump($db->errorInfo());

$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客系统</title>
    <link rel="stylesheet" type="text/css" href="../css/main(后台).css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="header_bc">
            <h1>博客管理系统</h1>
        </div>
        <div id="header_right">欢迎你：admin <a href="#">退出登录</a></div>
    </div>
    <div id="left">

        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">分类列表</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">

        <div id="add_head">
            <a href="category_add.php">增加分类</a>
        </div>
        <div id="table_list">
            <table border="" cellspacing="0" cellpadding="">
                <tr>
                    <th>分类id</th>
                    <th>分类名称</th>
                    <th>增加时间</th>
                    <th>修改时间</th>
                    <th>操作</th>

                </tr>
                <tr>
                    <td>1</td>
                    <td>html</td>

                    <td>2021-03-18 00：00：00</td>
                    <td>2021-03-18 00：00：00</td>
                    <td><a href="#">编辑</a>&nbsp;<a href="#">删除</a></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>css</td>

                    <td>2021-03-18 00：00：00</td>
                    <td>2021-03-18 00：00：00</td>
                    <td><a href="#">编辑</a>&nbsp;<a href="#">删除</a></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>java</td>

                    <td>2021-03-18 00：00：00</td>
                    <td>2021-03-18 00：00：00</td>
                    <td><a href="#">编辑</a>&nbsp;<a href="#">删除</a></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>数据库</td>

                    <td>2021-03-18 00：00：00</td>
                    <td>2021-03-18 00：00：00</td>
                    <td><a href="#">编辑</a>&nbsp;<a href="#">删除</a></td>
                </tr>

            </table>
        </div>


    </div>
</div>
</body>
</html>
