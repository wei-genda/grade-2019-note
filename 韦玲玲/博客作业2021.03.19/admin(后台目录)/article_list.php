<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/23
 * Time: 10:44
 */
?>;
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客系统</title>
    <link rel="stylesheet" type="text/css" href="../css/main(后台).css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="header_bc">
            <h1>博客管理系统</h1>
        </div>
        <div id="header_right">欢迎你：admin <a href="#">退出登录</a></div>
    </div>
    <div id="left">

        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">分类列表</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="miaobaoxie">
            <ul>
                <a href="#">首页</a>&gt;
                <a href="#">文章管理</a>&gt;
                <a href="#">文章列表</a>
            </ul>
        </div>
        <div id="table_header">
            <button class="btn">全选</button>
            <a href="#">删除选中任务</a>
            <a href="article_add.php" id="addnew" name="addName">增加新闻</a>
        </div>
        <div id="table_list">
            <table border="" cellspacing="0" cellpadding="">
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>标题</th>
                    <th>分类</th>
                    <th>简介</th>
                    <th>发表时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <tr>
                    <td><input type="checkbox" name="" id="" value="" /></td>
                    <td>1</td>
                    <td>vue新手入门指南</td>
                    <td>web前端</td>
                    <td>vue入门指南，适合新手</td>
                    <td>2021-03-18 00：00：00</td>
                    <td>2021-03-18 00：00：00</td>
                    <td><a href="#">编辑</a></td>
                </tr>
            </table>
        </div>


    </div>
</div>
</body>
</html>
