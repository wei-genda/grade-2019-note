<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/23
 * Time: 10:44
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客系统</title>
    <link rel="stylesheet" type="text/css" href="../css/main(后台).css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="header_bc">
            <h1>博客管理系统</h1>
        </div>
        <div id="header_right">欢迎你：admin <a href="#">退出登录</a></div>
    </div>
    <div id="left">

        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">分类列表</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">


        <div id="table_add">
            <form action="" method="post">
                <table class="table" border="" cellpadding="" cellspacing="0">
                    <tr>
                        <td id="a">分类名称:</td>
                        <th id="b"><input type="text"></th>
                    </tr>

                    <tr>
                        <td id="a">分类说明:</td>
                        <th id="b">
                            <textarea></textarea>
                        </th>
                    </tr>
                    <tr>
                        <td></td>
                        <!-- <td></td> -->
                        <th id="b">
                            <input class="btn" type="submit" value="保存" />&nbsp;&nbsp;
                            <input class="btn" type="reset" value="重置" />
                        </th>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>
