-- 建库

CREATE DATABASE IF NOT EXISTS eshop
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

 
-- 建表
CREATE TABLE item(
item_id int(11) UNSIGNED not NULL auto_increment,
item_name VARCHAR(100) not null COMMENT '商品名称',
item_price DECIMAL(10,2) NOT NULL COMMENT '商品价格',
PRIMARY KEY(item_id)
)ENGINE=innodb DEFAULT charset=utf8mb4 COMMENT='商品表';

-- 建表
CREATE TABLE cart(
cart_id int(11) UNSIGNED not NULL auto_increment,
user_id int(11) UNSIGNED NOT null COMMENT '用户id',
item_id int(11) UNSIGNED not null COMMENT'商品id',
item_num int(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT '商品数量',
add_time int(11) UNSIGNED not null DEFAULT '0' COMMENT '加入时间',
PRIMARY KEY (cart_id)
)ENGINE=innodb DEFAULT charset=utf8mb4 COMMENT '购物车表';



-- 商品表： 插入数据
insert into item
( item_name,item_price)
values('小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK',1349),
('TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',2199),
('海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视',1349),
('海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能',2699),
('TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机',1999),
('长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）',2099);

-- 购物车：插入数据
insert into cart
(user_id,item_id,item_num)
values 
(2,2,2),
(3,4,1),
(3,5,1),
(4,1,2);



-- 查询商品表所有数据。
select * from item;
-- 查询价格小于2000的商品。
select * from item where item_price <2000;
-- 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';
-- 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3;

-- 查询商品表，假设每页3条记录，查询第2页的数据。
select *from item limit 3 offset 3;
-- 查询所有商品，按照价格进行升序排序。
select * from item order by item_price asc;
-- 查询价格小于2000的商品总数。
select count(*)from item where item_price <2000;
-- 查询购物车中所有数据。
select * from cart;
-- 查询每个用户加入购物车的商品总数。
select user_id,SUM(item_num) from cart group by user_id;
-- 查询加入购物车的商品总数小于2的用户。
select user_id from cart group by user_id having sum(item_num)<2;
-- 查询有加入购物车的商品。
select distinct item.*from item inner join cart on item.item_id=cart.item_id;
-- 查询没有加入购物车的商品。
SELECT item.*FROM item LEFT JOIN cart ON item.item_id = cart.item_id WHERE ISNULL(cart.cart_id);

-- 删除数据
-- 用户4将商品1从购物车中移除。
delete from cart where user_id =4 and item_id=1;
-- 用户3清空购物车。
DELETE from cart where user_id=3;
-- 移除购物车中的所有数据：
-- 使用delete进行删除
delete from cart;
-- 使用truncate进行删除
TRUNCATE table cart;

-- 对比delete和truncate的差别

--  当仍要保留该表，但要删除所有记录时，用 TRUNCATE；
--  当要删除部分记录时，用 DELETE。





