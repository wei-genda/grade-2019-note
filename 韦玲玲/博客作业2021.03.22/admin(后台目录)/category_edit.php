<?php
//判断是否为空
$categoryId = $_GET['category_id'] ?? '';
//var_dump($categoryId);
//exit();
if (empty($categoryId)) {
    echo "参数错误<br>";
    echo "<a href='category_list.php'>返回列表页面</a>";
    exit();
}
////保存到数据库
$dsn = "mysql:host=127.0.0.1; dbname=blog";
$db =new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");
//
$sql = "select * from category where category_id = '".$categoryId."'";
$result = $db->query($sql);
$categoryInfo = $result->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客系统</title>
    <link rel="stylesheet" type="text/css" href="../css/main(后台).css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="header_bc">
            <h1>博客管理系统</h1>
        </div>
        <div id="header_right">欢迎你：admin <a href="#">退出登录</a></div>
    </div>
    <div id="left">

        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="category_list.php">分类列表</a></li>
<!--            <li><a href="#">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <div id="miaobaoxie">
            <ul>
                <a href="category_list.php">首页</a>&gt;
                <a href="category_list.php">分类管理</a>&gt;
                <a href="category_list.php">编辑分类</a>
            </ul>
        </div>

        <div id="table_add">
            <form action="category_edit_save.php" method="post">
                <table class="table" border="" cellpadding="" cellspacing="0">
                    <tr>
                        <td>分类id:</td>
                        <th><input type="text" value="<?php echo $categoryInfo['category_id'];?>"readonly="readonly" name="category_id"/></th>
                    </tr>
                    <tr>
                        <td id="a">分类名称:</td>
                        <th id="b"><input type="text" name="category_name" value="<?php echo $categoryInfo['category_name'];?>"></th>
                    </tr>

                    <tr>
                        <td id="a">分类描述:</td>
                        <th id="b">
                            <textarea name="category_desc"><?php echo $categoryInfo['category_desc'];?></textarea>
                        </th>
                    </tr>
                    <tr>
                        <td></td>

                        <th id="b">
                            <input class="btn" type="submit" value="保存"  />&nbsp;&nbsp;
                            <input class="btn" type="reset" value="重置" />
                        </th>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
</body>
</html>
