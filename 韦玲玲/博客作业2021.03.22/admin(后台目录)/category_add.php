<?php

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客系统</title>
    <link rel="stylesheet" type="text/css" href="../css/main(后台).css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="header_bc">
            <h1>博客管理系统</h1>
        </div>
        <div id="header_right">欢迎你：admin <a href="#">退出登录</a></div>
    </div>
    <div id="left">

        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="category_list.php">分类列表</a></li>
<!--            <li><a href="#">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">


        <div id="table_add">
            <form action="category_add_save.php" method="post">
                <table class="table" border="" cellpadding="" cellspacing="0">
                    <tr>
                        <td id="a">分类名称:</td>
                        <th id="b"><input type="text" name="category_name"></th>
                    </tr>

                    <tr>
                        <td id="a">分类描述:</td>
                        <th id="b">
                            <textarea name="category_desc"></textarea>
                        </th>
                    </tr>
                    <tr>
                        <td></td>

                        <th id="b">
                            <input class="btn" type="submit" value="保存"  />&nbsp;&nbsp;
                            <input class="btn" type="reset" value="重置" />
                        </th>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>
