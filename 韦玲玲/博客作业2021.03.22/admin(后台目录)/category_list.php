<?php
//date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1; dbname=blog";
$db =new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$sql = "select * from category order by `category_id` desc ";
$result = $db->query($sql);
//var_dump($db->errorInfo());
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>博客系统</title>
    <link rel="stylesheet" type="text/css" href="../css/main(后台).css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <div id="header_bc">
            <h1>博客管理系统</h1>
        </div>
<!--        <div id="header_right">欢迎你：admin <a href="#">退出登录</a></div>-->
    </div>
    <div id="left">

        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="category_list.php">分类列表</a></li>
<!--            <li><a href="#">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">

        <div id="add_head">
            <a href="category_add.php">增加分类</a>
        </div>
        <div id="table_list">
            <form method="post">
                <table border="" cellspacing="0" cellpadding="">
                    <tr>
                        <th></th>
                        <th>分类id</th>
                        <th>分类名称</th>
                        <th>分类描述</th>
                        <th>增加时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $item): ?>
                    <tr>
                        <td><input type="checkbox" name="category_id[]" value="<?php echo $item['category_id']?>"></td>
                        <td><?php echo $item['category_id']?></td>
                        <td><?php echo $item['category_name']?></td>
                        <td><?php echo $item['category_desc']?></td>
                        <td><?php echo date("y-m-d h:is:s",$item['update_time']);?></td>
                        <td><?php echo date("y-m-d h:is:s",$item['add_time'])?></td>
                        <td><a href="category_edit.php?category_id=<?php echo $item['category_id'];?>">编辑</a>&nbsp;
                            <a href="category_delete.php?category_id=<?php echo $item['category_id'];?>">删除</a></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </form>
        </div>


    </div>
</div>
</body>
</html>
