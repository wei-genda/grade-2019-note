-- 创建库test，并判断是否存在
create database if not exists test;


-- 创建员工信息表
use test;

create table employee (
	id int unsigned not null auto_increment comment '员工编号',
	name varchar(25) not null comment '员工名称',
	deptId int(11)  not null default 0 comment '所在部门编号',
	salary float(11,2) unsigned not null default 0 comment '工资',
	update_time int(11) unsigned not null comment '修改时间', 
	add_time int(11) unsigned not null comment '增加时间',
	primary key (`id`)
) engine = innodb default charset = utf8mb4 comment = '员工信息表';



-- 创建部门信息表
use test;

create table department (
	deptId int unsigned not null auto_increment comment '部门编号',
	name varchar(25) not null comment '部门名称',
	level int(11)  not null default 0 comment '部门等级',
	parentDetId int(11) unsigned not null default 0 comment '上级部门编号',
	deptLeader int(11) unsigned not null default 0 comment '部门领导',
	update_time int(11) unsigned not null comment '修改时间', 
	add_time int(11) unsigned not null comment '增加时间',
	primary key (`deptId`)
) engine = innodb default charset = utf8mb4 comment = '部门信息表';


-- 修改表名 employee改为employee_info
alter table employee rename to employee_info;

-- 修改表名 department改为department_info
alter table department rename to department_info;


--     员工信息表增加如下字段：
-- 
-- 字段名称 	   数据类型 	     其他要求 	                   备注
-- sex 	         tinyint(3) 	   无符号，默认值为3  	         员工性别：1男2女3保密
-- address 	     VARCHAR(100) 	 不允许为空，默认值空字符串 	 住址
-- join_time     int(11) 	       不允许为空 	                 入职时间

alter table employee_info add sex tinyint(3) unsigned default 3 after name;

alter table employee_info add address varchar(100) not null default ' ' after sex;

alter table employee_info add join_time int(11) not null after salary;

show create table employee_info;



-- 员工信息表工资salary字段进行修改：
-- 
-- 字段名称 	数据类型      	其他要求 	                          备注
-- salary 	  decimal(11,2) 	无符号，不允许为空，默认值为0     	工资

alter table employee_info change salary salary decimal(11,2) unsigned not null default 0 comment '工资';

