CREATE DATABASE if not exists blog;     #新建博客数据库blog。


CREATE DATABASE if not exists student   #新建学生数据库student，指定其默认字符集为 utf8mb4，默认校对规则为 utf8mb4_general_ci。
	DEFAULT CHARACTER SET utf8mb4
	DEFAULT COLLATE  utf8mb4_general_ci;

create database if not exists eshop;    #新建电子商务数据库eshop。


drop database if exists blog;           #删除上面新建的库
drop database if exists student;
drop database if exists eshop;