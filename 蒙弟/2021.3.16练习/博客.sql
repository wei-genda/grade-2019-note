-- 创建博客库 blog
CREATE DATABASE IF NOT EXISTS blog
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;


-- 创建分类表 category
use blog;

create table category (
	category_id int(10) unsigned not null AUTO_INCREMENT,
	category_name varchar(80) not null comment '分类名称',
	category_desc varchar(1000) not null comment '分类说明',
	add_time int(10) not null comment '增加时间',
	update_time int(10) not null comment '修改时间',
	primary key (`category_id`)
)engine = innodb default charset = utf8mb4 comment = '分类表'

SELECT * FROM category;

-- 创建文章表 article
create table article(
	article_id int(11) unsigned not null AUTO_INCREMENT,
	article_title varchar(100) not null comment '文章标题',
	category_id int(11) not null comment '文章分类',
	article_author varchar(20) comment '文章作者',
	content longtext not null comment '文章内容',
	add_time int(11) not null comment '增加时间',
	update_time int(11) not null comment '修改时间',
	primary key (`article_id`)
)engine = innodb default charset = utf8mb4 comment = '文章表'

SELECT * FROM article;