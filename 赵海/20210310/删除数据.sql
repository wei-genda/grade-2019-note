-- 1. 用户4将将商品1从购物车中移除。
delete from cart where user_id=4 and item_id=1;

-- 2. 用户3清空购物车。
delete from cart where user_id=3;

-- 3. 移除购物车中的所有数据：
-- 	1. 使用delete进行删除
delete from cart;

-- 	2. 使用truncate进行删除
truncate table cart;

-- 	3. 对比delete和truncate的差别  一个从删除后的数据开始，一个从零开始。