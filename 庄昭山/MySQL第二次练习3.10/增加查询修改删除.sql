CREATE TABLE `item` (
	`item_id` INT ( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
	`item_name` VARCHAR ( 100 ) NOT NULL COMMENT '商品名称',
	`item_price` DECIMAL ( 10, 2 ) NOT NULL COMMENT '商品价格',
	PRIMARY KEY ( `item_id` ) 
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4 COMMENT = '商品表';
CREATE TABLE `cart` (
	`cart_id` INT ( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT ( 11 ) UNSIGNED NOT NULL COMMENT '用户id',
	`item_id` INT ( 11 ) UNSIGNED NOT NULL COMMENT '商品id',
	`item_num` INT ( 11 ) UNSIGNED NOT NULL DEFAULT '1' COMMENT '商品数量',
	`add_time` INT ( 11 ) UNSIGNED NOT NULL DEFAULT '0' COMMENT '加入时间',
	PRIMARY KEY ( `cart_id` ) 
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;
INSERT INTO item ( item_name, item_price )
VALUES
	( '小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK ', 1349 );
INSERT INTO item ( item_name, item_price )
VALUES
	( 'TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机', 2199 ),
	( '海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视', 1349 ),
	( '海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能', 2699 ),
	( 'TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机', 1999 ),
	( '长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）', 2099 );
INSERT INTO cart ( user_id, item_id, item_num, add_time )
VALUES
	(
		1,
		1,
		1,
	unix_timestamp(
	now()));
	
	SELECT * FROM item;
	
-- 	查询价格小于2000的商品：
SELECT * FROM item WHERE item_price < 2000;

-- 查询名称中包含 海信 的商品：
SELECT * FROM item WHERE item_name like '%海信%';

-- 假设每页2条记录，查询第1页的数据：
SELECT * FROM item LIMIT 2;

-- 假设每页2条记录，查询第2页的数据：
SELECT * FROM item LIMIT 2 OFFSET 2;

-- 假设每页2条记录，查询第3页的数据：
SELECT * FROM item LIMIT 2 OFFSET 4;

-- 查询所有商品，按照价格进行降序排序
SELECT * FROM item ORDER BY item_price DESC;

-- 查询商品总数：
SELECT count(*) FROM item;

-- 查询购物车中所有数据：
SELECT * FROM cart;

-- 查询每个用户加入购物车的商品总数：
SELECT user_id, sum(item_num) FROM cart group by user_id;

-- 查询加入购物车的商品总数小于2的用户：
SELECT user_id FROM cart group by user_id HAVING sum(item_num) < 2;

-- 查询有加入购物车的商品：
-- 连接查询，DISTINCT表示去除重复的意思
SELECT DISTINCT item.* FROM item INNER JOIN cart on item.item_id = cart.item_id;

-- 子查询方式，不推荐，mysql性能比较低
SELECT * FROM item where item_id in (SELECT item_id FROM cart);

-- 查询没有加入购物车的商品：
SELECT	item.* FROM	item LEFT JOIN cart ON item.item_id = cart.item_id WHERE	ISNULL(cart.cart_id);

-- 修改商品表，商品id为1的价格为1249：
UPDATE item SET item_price=1249 WHERE item_id=1;

-- 用户3多加了一件商品5到购物车：
UPDATE cart set item_num=item_num+1 WHERE user_id=3 AND item_id=5;

-- 用户4将将商品1从购物车中移除：
DELETE FROM cart WHERE user_id=4 AND item_id=1; 

-- 移除购物车中的所有数据：
DELETE FROM cart;

-- 使用 TRUNCATE 语句清空item表数据：
TRUNCATE TABLE item;