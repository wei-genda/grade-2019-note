<?php
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec('set names utf8mb4');
$sql = "SELECT * FROM category order by category_id asc";
$statement = $db->query($sql);
$categoryList = $statement->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("PRC");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>管理系统</title>
    <link href="css/main.css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <div id="left">
        <div id="left_top">
            <h1>博客管理系统</h1>
        </div>
        <div id="center">
            <ul>
                <li><a href="category_list.php">分类管理</a></li>
                <li><a href="article_list.php">新闻管理</a></li>
                <li><a href="article_list.php">新闻分类</a></li>
            </ul>
        </div>
    </div>
    <div id="right">
        <div id="right_top">
            <p>欢迎您：Admin <span><a href="#">退出登入</a></span></p>
        </div>
        <div id="right_content">
            <div id="right_content_top">
                <a href="#">首页</a>
                &gt
                <a href="#">分类管理</a>
                &gt
                <a href="#">分类列表</a>
            </div>
            <div id="right_content_center">
							<span>
								<input type="button" value="全选" id="chose_all" style="width: 60px;height: 25px; border: #EEE8E1;border-color: #FAF7F5;;" />
								<a href="#">删除选中任务</a>
								<a href="category_add.php" id="newAdd">增加新闻</a>
							</span>
            </div>
            <div id="bottom">
                <table class="list">
                    <tr>
                        <th></th>
                        <th>分类ID</th>
                        <th>分类名称</th>
                        <th>简介</th>
                        <th>发表时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $item): ?>
                    <tr>
                        <td><input type="checkbox" name="chose" ></td>
                        <td><?php echo $item['category_id']?></td>
                        <td><?php echo $item['category_name']?></td>
                        <td><?php echo $item['category_desc']?></td>
                        <td><?php echo date("Y-m-d H:i:s",$item['update_time'])?></td>
                        <td><?php echo date("Y-m-d H:i:s",$item['add_time'])?></td>
                        <td>
                            <a href="category_edit.php?categoryId=<?php echo $item['category_id']?>">编辑</a>
                            <a href="category_delete.php?categoryId=<?php echo $item['category_id']?>">删除</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>

