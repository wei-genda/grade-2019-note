<?php
$category_id=$_POST['category_id'];
$category_name=$_POST['category_name'];
$category_desc=$_POST['category_desc'];
if(strlen($category_name)<5 || strlen($category_name)>50){
    echo "分类名称应在5~50间";
    echo "<a href='javascript:void(0);history.back()'>返回</a>";
    exit();
}elseif (strlen($category_desc)<50 || strlen($category_desc)>1000){
    echo "分类简介应在50~1000之间";
    echo "<a href='javascript:void(0);history.back()'>返回</a>";
    exit();
}
$update_time=time();
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");
$sql = "update category 
        set category_name='$category_name',category_desc='$category_desc',update_time='$update_time'
        where category_id={$category_id}";
$statement = $db->exec($sql);
if ($statement){
    echo "编辑成功";
    echo "<a href='category_list.php'>返回列表</a>";
}else {
    echo "编辑失败" . $db->errorInfo()[2];
    echo "<a href='category_edit.php?category_id={$category_id}'>返回</a>";
}
