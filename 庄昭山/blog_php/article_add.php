<?php
$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec('set names utf8mb4');
$sql = "SELECT * FROM article order by article_id asc";
$sql1 = "SELECT * FROM category order by category_id asc";
$statement = $db->query($sql);
$statement1 = $db->query($sql1);
$articleList = $statement->fetch(PDO::FETCH_ASSOC);
$categoryList = $statement1->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("PRC");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/add_list.css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <div id="left">
        <div id="left_top">
            <h1>博客管理系统</h1>
        </div>
        <div id="center">
            <ul>
                <li><a href="category_list.php">分类管理</a></li>
                <li><a href="article_list.php">新闻管理</a></li>
                <li><a href="article_list.php">新闻分类</a></li>
            </ul>
        </div>
    </div>
    <div id="right">
        <div id="right_top">
            <p>欢迎您：Admin <span><a href="#">退出登入</a></span></p>
        </div>
        <div id="right_content">
            <div id="right_content_top">
                <a href="#">首页</a>
                &gt
                <a href="#">新闻管理</a>
                &gt
                <a href="#">新闻列表</a>
            </div>
            <div id="bottom">
                <form action="article_add_save.php" method="post">
                    <table class="list1">
                        <input type="hidden" name="article_id" />
                        <tr>
                            <td>文章标题：</td>
                            <td><input type="text" name="article_title" value=""/></td>
                        </tr>
                        <tr>
                            <td>分类名称：</td>
                            <td>
                                <select name="category_id">
                                    <?php foreach ($categoryList as $value): ?>
                                    <option value="<?php echo $value['category_id']?>">
                                        <?php echo $value['category_name']?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>文章内容：</td>
                            <td>
                                <textarea rows="6" cols="50" name="content"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td><input type="submit" value="提交" > <input type="reset" value="重置"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>


