<?php

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/add_list.css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <div id="left">
        <div id="left_top">
            <h1>博客管理系统</h1>
        </div>
        <div id="center">
            <ul>
                <li><a href="category_list.php">分类管理</a></li>
                <li><a href="article_list.php">新闻管理</a></li>
                <li><a href="article_list.php">新闻分类</a></li>
            </ul>
        </div>
    </div>
    <div id="right">
        <div id="right_top">
            <p>欢迎您：Admin <span><a href="#">退出登入</a></span></p>
        </div>
        <div id="right_content">
            <div id="right_content_top">
                <a href="#">首页</a>
                &gt
                <a href="#">新闻管理</a>
                &gt
                <a href="#">新闻列表</a>
            </div>
            <div id="bottom">
                <form action="category_add_save.php" method="post">
                    <table class="list1">
                        <tr>
                            <td>分类名称：</td>
                            <td><input type="text" name="category_name"></td>
                        </tr>
                        <tr>
                            <td>分类简介：</td>
                            <td>
                                <textarea rows="6" cols="50" name="category_desc"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td><input type="submit" value="提交" > <input type="reset" value="重置"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>


