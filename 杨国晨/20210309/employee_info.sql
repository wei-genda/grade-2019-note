CREATE TABLE `employee_info` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `name` varchar(25) NOT NULL COMMENT '员工名称',
  `deptid` int NOT NULL DEFAULT '0' COMMENT '所在部门编号',
  `salary` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '工资',
  `update_time` int NOT NULL COMMENT '修改时间',
  `add_tiem` int NOT NULL COMMENT '增加时间',
  `sex` tinyint DEFAULT '3' COMMENT '员工性别：1男2女3保密',
  `join_time` int NOT NULL COMMENT '入职时间',
  `address` varchar(100) NOT NULL DEFAULT ' ' COMMENT '住址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='员工信息表';