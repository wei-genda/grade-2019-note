CREATE TABLE `department_info` (
  `deptid` int NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `name` varchar(25) NOT NULL COMMENT '部门名称',
  `level` int NOT NULL DEFAULT '0' COMMENT '部门等级',
  `parentDeptid` int NOT NULL DEFAULT '0' COMMENT '上级部门编号',
  `deotLeader` int NOT NULL DEFAULT '0' COMMENT '部门领导',
  `update_time` int NOT NULL COMMENT '修改时间',
  `add_time` int NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`deptid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门信息表';