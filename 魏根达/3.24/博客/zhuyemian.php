<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=get";
$db = new pDo($dsn ,"root","123456");
$sql = "SELECT * FROM bok ORDER BY category_id DESC";
$result = $db->query($sql);
$categoryList = $result -> fetchAll(pDo::FETCH_ASSOC);


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台首页</title>
    <link rel="stylesheet" type="text/css" href="zycss/base.css">
    <link rel="stylesheet" type="text/css" href="zycss/left.css">
    <link rel="stylesheet" type="text/css" href="zycss/right.css">
    <link rel="stylesheet" type="text/css" href="zycss/top.css">
</head>
<body>
<div id="cont">
    <div id="top">
        <div id="top_a">
            <h1>博客管理系统</h1>
        </div>
        <div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right_a">
            <a href="#">首页</a>&gt;
            <a href="#">文章管理</a>&gt;
            <a href="#">分类列表</a>
        </div>
        <div id="right_b">
            <button id="submin">全选</button>
            <a href="#">删除选中任务</a>
            <a id="a" href="zengjiayemian.php">增加分类</a>
        </div>
        <div id="right_c">
            <table border="">

                <tr>
                    <th></th>
                    <th>分类id</th>
                    <th>分类名称</th>
                    <th>增加时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($categoryList as $row):?>
                    <tr>
                        <td><input type="checkbox" class="checkbox"></td>
                        <td><?php echo $row['category_id'];?></td>
                        <td><?php echo $row['category_name'];?></td>
                        <td><?php echo date("Y-m-d H:i:s",$row['add_time']);?></td>
                        <td><?php echo date("Y-m-d H:i:s",$row['update_time']);?></td>
                        <td><a href="bianji.php?categoryid=<?php echo $row['category_id']; ?>">编辑</a>

                        <a href="shanchu.php?categoryid=<?php echo $row['category_id']; ?>">删除</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
</body>
</html>


