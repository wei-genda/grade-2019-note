/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : get

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-23 17:50:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bok
-- ----------------------------
DROP TABLE IF EXISTS `bok`;
CREATE TABLE `bok` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT '',
  `category_desc` varchar(255) DEFAULT NULL,
  `update_time` int(11) NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bok
-- ----------------------------
INSERT INTO `bok` VALUES ('1', 'php&&mysqlrrrrrrrr', 'css', '1616481729', '1616481729');
INSERT INTO `bok` VALUES ('2', 'php', 'css', '1616483343', '1616483343');
INSERT INTO `bok` VALUES ('3', '你是', 'java', '0', '0');
INSERT INTO `bok` VALUES ('4', 'html', 'java', '1616488316', '1616488316');
INSERT INTO `bok` VALUES ('5', 'dgfdgfdaerfwea', '\r\n		aaaaaa					', '1616490994', '1616490994');
INSERT INTO `bok` VALUES ('6', 'bbb', 'bbbbb\r\n							', '1616491034', '1616491034');
INSERT INTO `bok` VALUES ('7', 'aaaaaa', '\r\n		bbbbbb					', '1616491153', '1616491153');
INSERT INTO `bok` VALUES ('8', '', '\r\n							', '1616492269', '1616492269');
