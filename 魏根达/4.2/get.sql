/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : get

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-04-02 12:03:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `Admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Admin_name` varchar(255) NOT NULL,
  `Admin_content` varchar(255) NOT NULL,
  `Admin_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'æŽå››', 'awfawf', '1617336034');

-- ----------------------------
-- Table structure for bok
-- ----------------------------
DROP TABLE IF EXISTS `bok`;
CREATE TABLE `bok` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT '',
  `category_desc` varchar(255) DEFAULT NULL,
  `update_time` int(11) NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bok
-- ----------------------------
INSERT INTO `bok` VALUES ('1', 'php&&mysqlrrrrrrrr', 'css', '1616481729', '1616481729');
INSERT INTO `bok` VALUES ('2', 'php', 'css', '1616483343', '1616483343');
INSERT INTO `bok` VALUES ('3', '你是', 'java', '0', '0');
INSERT INTO `bok` VALUES ('4', 'html', 'java', '1616488316', '1616488316');
INSERT INTO `bok` VALUES ('9', '', '', '1617330313', '1617330313');
INSERT INTO `bok` VALUES ('10', '', '\r\n							', '1617330364', '1617330364');
INSERT INTO `bok` VALUES ('11', '', '\r\n							', '1617330426', '1617330426');
INSERT INTO `bok` VALUES ('12', '', '\r\n							', '1617330555', '1617330555');
INSERT INTO `bok` VALUES ('13', '44', '\r\n	44						', '1617333420', '1617333420');
INSERT INTO `bok` VALUES ('14', '44', '\r\n		ee					', '1617333600', '1617333600');
INSERT INTO `bok` VALUES ('15', '44', '\r\n	rfg						', '1617334446', '1617334446');
INSERT INTO `bok` VALUES ('16', '', '\r\n							', '1617334608', '1617334608');

-- ----------------------------
-- Table structure for reg
-- ----------------------------
DROP TABLE IF EXISTS `reg`;
CREATE TABLE `reg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mima` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of reg
-- ----------------------------
INSERT INTO `reg` VALUES ('1', '李四', '123456');
INSERT INTO `reg` VALUES ('2', '赵三', '123456');
