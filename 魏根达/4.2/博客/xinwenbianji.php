<?php
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>新闻增加页面</title>
		<link rel="stylesheet" type="text/css" href="zecss/css/base.css">
		<link rel="stylesheer" type="text/css" href="zecss/css/top.css">
		<link rel="stylesheet" type="text/css" href="zecss/css/left.css">
		<link rel="stylesheet" type="text/css" href="zecss/css/right.css">
	</head>
	<body>
		<div id="cont">
			<div id="top">
				<div id="top_a">
					<h1>博客管理系统</h1>
				</div>
				<div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
			</div>
			<div id="left">
				<ul>
					<li><a href="#">分类管理</a></li>
					<li><a href="#">新闻管理</a></li>
					<li><a href="#">管理员</a></li>
				</ul>
			</div>
			<div id="right">
				<div id="right_a">
					<a href="#">首页</a>&gt;
					<a href="#">新闻管理</a>&gt;
					<a href="#">新闻列表</a>
				</div>
				<div id="right_b">
					<table border="">
						<tr>
							<td  width="190px">新闻id ：</td>
							<td><input type="text" id="a" /></td>
						</tr>
						<tr>
							<td>新闻标题：</td>
							<td><input type="text" id="b"/></td>
						</tr>
						<tr>
							<td>新闻分类：</td>
							<td><select id="c">
							<option>web前端</option>
							<option>服务器</option>
							<option>数据库</option>
							</select></td>
						</tr>
						<tr>
							<td>新闻内容：</td>
							<td><textarea rows="" cols="" id="d">

							</textarea></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" value="提交" class="but" />
								<input type="reset" value="重置" class="but" />
							</td>
						</tr>
					</table>
				</div>

			</div>
		</div>
        <script src="js/main.js"></script>
	</body>
</html>
