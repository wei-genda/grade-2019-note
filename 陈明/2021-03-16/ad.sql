CREATE TABLE `article` (
  `article_id` int(11) NOT NULL COMMENT '文章id',
  `article_title` varchar(100) NOT NULL COMMENT '文章标题',
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_author` varchar(20) DEFAULT NULL COMMENT '作者',
  `article_content` text COMMENT '文章内容',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL COMMENT '文章id',
  `article_title` varchar(100) NOT NULL COMMENT '文章标题',
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_author` varchar(20) DEFAULT NULL COMMENT '作者',
  `article_content` text COMMENT '文章内容',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;