<?php
/**
 * 分类增加功能
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客</title>
    <link href="css/top2.css" rel="stylesheet" type="text/css" />
    <link href="css/left2.css" rel="stylesheet" type="text/css" />
    <link href="css/right2.css" rel="stylesheet" type="text/css" />
    <link href="css/bottom2.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top2">
    <p style="color:#FFF"><strong>博客管理系统</strong></p>
    <li style="float:right; font-weight:100;"><a href="#">退出登录</a></li>
    <li style="float:right"><strong>欢迎你：admin&nbsp;</strong></li>
    <div id="t1">
    </div>
</div>


<div id="left2">
    <ul>
        <li style="padding:70px 0 0 0;"><a href="category_list.php" style="text-decoration:none; color:#000">分类管理</a></li>
        <li><a href="article_list.php">文章管理</a></li>
        <li><a href="#" style="text-decoration:none; color:#000">管理员</a></li>
    </ul>
</div>

<div id="right2">
    <ul>
        <li><a href="#">首页</a>><a href="#">分类管理</a>><a href="#">增加分类</a></li>

    </ul>
</div>

<div id="bottom2">
    <form action="category_add_save.php" method="post">
        <table width="800" border="1" cellspacing="0">
            <tr>
                <td>&nbsp;分类名称：</td>
                <td>&nbsp;<input type="text" name="category_name"/></td>
            </tr>
            <tr>
                <td>&nbsp;分类描述：</td>
                <td>&nbsp;<textarea rows="15" cols="60" name="category_desc"></textarea></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;<input type="submit"/>&nbsp;<input type="reset"/></td>
            </tr>
        </table>
    </form>
</div>

</body>

</html>

