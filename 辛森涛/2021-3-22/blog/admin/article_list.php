<?php
date_default_timezone_set("PRc");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM article order by article_id desc ";
$result = $db->query($sql);
$articleList = $result->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客</title>
    <link href="css/top.css" rel="stylesheet" type="text/css" />
    <link href="css/left.css" rel="stylesheet" type="text/css" />
    <link href="css/right.css" rel="stylesheet" type="text/css" />
    <link href="css/bottom.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id = "base">
    <div id="top">
        <p style="color:#FFF"><strong>博客管理系统</strong></p>
        <li style="float:right; font-weight:100;"><a href="#">退出登录</a></li>
        <li style="float:right"><strong>欢迎你：admin&nbsp;</strong></li>
        <div id="t1">
        </div>
    </div>
</div>

<div id="left">
    <ul>
        <li style="padding:70px 0 0 0;"><a href="#">分类管理</a></li>
        <li><a href="#">新闻管理</a></li>
        <li><a href="#">管理员</a></li>
    </ul>
</div>

<div id="right">
    <ul>
        <li><a href="#">首页</a>><a href="#">新闻管理</a>><a href="#">新闻列表</a></li>
        <li><input type="button" value="全选" class="btnstyle" /> <a href="#" style="margin-left:-101px;">删除选中的任务</a><a href="#" style="margin-left:990px;">增加新闻</a></li>
    </ul>
</div>

<div id="bottom">
    <table width="1170" cellspacing="0" style="margin-left:215px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;<strong>ID</strong></td>
            <td>&nbsp;<strong>标题</strong></td>
            <td>&nbsp;<strong>分类</strong></td>
            <td>&nbsp;<strong>简介</strong></td>
            <td>&nbsp;<strong>发表时间</strong></td>
            <td>&nbsp;<strong>修改时间</strong></td>
            <td>&nbsp;<strong>操作</strong></td>
        </tr>
        <?php foreach ($articleList as $row): ?>
        <tr>
            <td><input type="checkbox" /></td>
            <td>&nbsp;<?php echo $row['article_id']; ?></td>
            <td>&nbsp;<?php echo $row['article_title']; ?></td>
            <td>&nbsp;<?php
//                echo $row['category_id'];
                $sql = "select * from category where category_id='{$row['category_id']}'";
                $result = $db->query($sql);
                $category = $result->fetch(PDO::FETCH_ASSOC);
                echo $category['category_name'];
                ?>
            </td>
            <td>&nbsp;<?php echo $row['content']; ?></td>
            <td>&nbsp;<?php echo date("Y-m-d H:i:s", $row['add_time']); ?></td>
            <td>&nbsp;<?php echo date("Y-m-d H:i:s", $row['update_time']); ?></td>
            <td>&nbsp;<a href="#" style="text-decoration:none;">编辑</a></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
</body>

</html>

