CREATE TABLE `category` (
  `CategoryId` int(11) NOT NULL,
  `CategoryName` varchar(20) NOT NULL COMMENT '分类名称',
  `CategoryDesc` varchar(10000) NOT NULL COMMENT '分类描述',
  `update_timt` int(11) NOT NULL COMMENT '修改时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `article` (
  `Article_Id` int(11) NOT NULL,
  `Article_title` varchar(100) NOT NULL COMMENT '文章标题',
  `Category_Id` int(11) NOT NULL COMMENT '所属分类',
  `Article_autor` varchar(20) NOT NULL COMMENT '文章作者',
  `Article_content` varchar(10000) NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
