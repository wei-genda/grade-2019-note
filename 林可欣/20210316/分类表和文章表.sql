create database blog
		DEFAULT CHARACTER SET utf8mb4
		DEFAULT COLLATE utf8mb4_general_ci;	

use blog;

create table category (
  category_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  category_name varchar(20) NOT NULL COMMENT '分类名称',
  category_desc varchar(1000) NOT NULL COMMENT '分类描述',
  update_time int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  add_time int(11) NOT NULL COMMENT '记录增加时间',
  primary key (category_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='分类表';

create table article (
  article_id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) NOT NULL COMMENT '所属分类',
  article_title varchar(100) NOT NULL COMMENT '标题',
  content longtext NOT NULL COMMENT '文章内容',
  update_time int(11) NOT NULL COMMENT '更新时间',
  add_time int(11) NOT NULL COMMENT '增加时间',
  primary key (article_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 comment='文章表';