-- 1. 查询商品表所有数
SELECT * FROM item;
-- 2. 查询价格小于2000的商品。
SELECT * FROM item where item_price < 2000;
-- 3. 查询名称中包含 海信 的商品。
SELECT * FROM item where item_name LIKE '%海信%';
-- 4. 查询商品表，假设每页3条记录，查询第1页的数据。
SELECT * from item LIMIT 2;
-- 5. 查询商品表，假设每页3条记录，查询第2页的数据。
SELECT * from item LIMIT 2 OFFSET 2;
-- 6. 查询所有商品，按照价格进行升序排序。
SELECT * from item ORDER BY item_price DESC;
-- 7. 查询价格小于2000的商品总数。
SELECT COUNT(*) FROM item;
-- 8. 查询购物车中所有数据。
SELECT * FROM cart;
-- 9. 查询每个用户加入购物车的商品总数。
SELECT user_id, sum(item_num) FROM cart group by user_id;
-- 10. 查询加入购物车的商品总数小于2的用户。
SELECT user_id FROM cart GROUP BY user_id HAVING SUM(item_num)<2;
-- 11. 查询有加入购物车的商品。
SELECT DISTINCT item.* FROM item INNER JOIN cart on item.item_id = cart.item_id;
-- 12. 查询没有加入购物车的商品。
SELECT
	item.*
FROM
	item
LEFT JOIN cart ON item.item_id = cart.item_id
WHERE
	ISNULL(cart.cart_id);
-- 1. 用户4将将商品1从购物车中移除。
DELETE FROM cart where user_id=4 AND item_id=1;
-- 2. 用户3清空购物车。

-- 3. 移除购物车中的所有数据：
DELETE FROM cart;