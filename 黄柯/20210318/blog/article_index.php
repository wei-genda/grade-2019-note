<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 10:29
 */


?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文章首页</title>
    <link rel="stylesheet" href="css/article_index.css" type="text/css" />
</head>

<body>
<div id="best">
    <div id="shang">
        <h2>非常日记</h2>
        <h6>一个有soul的博客</h6>
    </div>
    <div id="zhong">
        <a href="#">首页</a>
        <a href="#">html</a>
        <a href="#">css</a>
        <a href="#">数据库</a>
    </div>
    <div id="xia1">
        <h2>入门介绍</h2>
        <h6>2021-03-01&nbsp;15:00:00</h6>
        <ul>
            <li><h7>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</h7>
            </li>
        </ul>
        <input type="button" value="阅读全文" />
    </div>
    <div id="xia2">
        <h2>入门介绍</h2>
        <h6>2021-03-01&nbsp;15:00:00</h6>
        <ul>
            <li><h7>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</h7></li>
        </ul>
        <input type="button" value="阅读全文" />
    </div>
    <div id="xia3">
        <h2>入门介绍</h2>
        <h6>2021-03-01&nbsp;15:00:00</h6>
        <ul>
            <li>
                <h7>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</h7></li>
        </ul>
        <input type="button" value="阅读全文" />
    </div>
    <div id="zuizuixia">
        <ul>
            <li><a href="#">上一页</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">下一页</a></li>
        </ul>
    </div>
    <h8>copyright&nbsp;@&nbsp;非常日记</8>
</div>
</body>
</html>
