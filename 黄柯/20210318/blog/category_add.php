<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 10:29
 */


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客增加页</title>
    <link rel="stylesheet" href="css/article_add.css" type="text/css" />
</head>

<body>
<form action="category_add_save.php" method="post">
    <div id="top" style="width:1440px;">
        <div id="ttop" style="width:300px;float:left;">
            <h2>博客管理系统</h2>
        </div>
        <div id="rtop" style="width:1140px;float:left; font-size: 20px;text-decoration: none;color: azure;">
            <h5>欢迎你:<a href="#" style="color: white;text-decoration: none;">admin</a>&nbsp;<a href="#" style="color: white;text-decoration: none;">退出登录</a></h5>
        </div>
    </div>
    <div id="button">
        <div id="bleft">
            <ul>
                <li>
                    <a href="#" class="fenlei">分类管理</a>
                </li>
                <li>
                    <a href="#" class="xinwen">新闻管理</a>
                </li>
                <li>
                    <a href="#" class="guanli">管理员</a>
                </li>
            </ul>
        </div>
        <div id="bright">
            <a href="#">首页</a>&gt;<a href="#">新闻管理</a>&gt;<a href="category_list.php">新闻列表</a>

            <table border="1" cellspacing="0" width="1000px;" class="context">

                <tr>
                    <td style="text-align: center;">分类名称:</td>
                    <td><input type="text" name="article_name" /></td>
                </tr>
                <tr>
                    <td style="text-align: center;">分类描述:</td>
                    <td><textarea rows="10" cols="30" name="article_desc"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="提交" />
                        <input type="reset" value="重置" />
                    </td>
                </tr>

            </table>
        </div>
    </div>
</form>
</body>
</html>

