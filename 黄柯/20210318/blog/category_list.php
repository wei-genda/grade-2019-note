<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 10:29
 */

date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn , "root" , "123456");

$sql = "select * from category order by category_id asc ";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>新闻列表</title>
    <link href="css/article_list.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<form>
    <div id="top" style="width:1440px;">
        <div id="ttop" style="width:300px;float:left;"><h2>博客管理系统</h2></div>
        <div id="rtop" style="width:1140px;float:left; font-size: 20px;"><h5>欢迎你:<a href="#" style="color: white;text-decoration: none;">admin</a>&nbsp;<a href="#"style="color: white;text-decoration: none;">退出登录</a></h5></div>
    </div>
    <div id="button">
        <div id="bleft">
            <ul>
                <li>
                    <a href="#">分类管理</a>
                </li>
                <li>
                    <a href="#">新闻管理</a>
                </li>
                <li>
                    <a href="#">管理员</a>
                </li>
            </ul>
        </div>
        <div id="bright">
            <a href="#">首页</a>&gt;<a href="#">新闻管理</a>&gt;<a href="#">新闻列表</a>
            <input type="button" id="b" value="全选" />
            <a href="#">删除选中任务</a>
            <a href="category_add.php" class="zj">增加新闻</a>
            <table border="1" align="center" cellspacing="0" width="1110px;" >
                <tr>
                    <th> </th>
                    <th>ID</th>
                    <th>分类</th>
                    <th>简介</th>
                    <th>发表时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>

                <?php foreach ($categoryList as $item): ?>
                <tr align="center">
                    <td>
                        <input style="width:25px; vertical-align:middle; margin-top:0px;" type="checkbox" class="checkbox" />
                    </td>
                    <td><?php echo $item['category_id'];?></td>
                    <td><?php echo $item['category_name'];?></td>
                    <td><?php echo $item['category_desc'];?></td>
                    <td><?php echo $item['update_time'];?></td>
                    <td><?php echo $item['add_time'];?></td>
                    <td width="90px;">
                        <a href="category_list_edit.php?category_id=<?php echo  $item['category_id'] ?>">编辑</a>
                        <a href="category_list_delete.php?category_id=<?php echo  $item['category_id'] ?>">删除</a>
                    </td>
                </tr>
                <?php  endforeach;?>

            </table>
        </div>

    </div>


</form>
</body>
</html>

