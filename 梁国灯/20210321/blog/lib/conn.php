<?php
$GLOBALS['dsn'] = "mysql: host=127.0.0.1;dbname=blog";
$GLOBALS['db'] = new PDO($dsn, 'root', '123456');
function queryAll($sql) {
    $result = $GLOBALS['db']->query($sql);
    return $GLOBALS['list'] = $result->fetchAll(PDO::FETCH_ASSOC);
}
function execute($sql) {
    return $GLOBALS['result'] = $GLOBALS['db']->exec($sql);
}