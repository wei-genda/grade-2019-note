<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>增加文章</title>
    <link type="text/css" rel="stylesheet" href="../css/admin-main.css">
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div id="container">
    <div id="header">
        <h2>博客管理系统</h2>
        <div class="info">
            欢迎你：<span>admin</span>
            <a href="javascript:void(0);">退出登录</a>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php" class="action">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <!-- <li><a href="javascript:void(0);">管理员</a></li>  -->
        </ul>
    </div>
    <div id="right">
        <div class="breadcrumbs-nav">
            <a href="article_list.php">首页</a> &gt;
            <a href="category_list.php">分类管理</a> &gt;
            <a href="javascript:void(0);">增加分类</a>
        </div>
        <form action="javascript:void(0);" method="post">
            <table class="add">
                <tr>
                    <td>分类名称：</td>
                    <td><input type="text" name="category-name" id="category-name" /></td>
                </tr>
                <tr>
                    <td>分类描述：</td>
                    <td><textarea rows="15" cols="58" name="category-desc" id="category-desc"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="button" id="submit">提交</button>
                        <button type="reset">重置</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <script>
        $(function() {
            $('#submit').click(function() {
                let name = $('#category-name').val();
                let desc = $('#category-desc').val();
                $.post(
                    'category_add_check.php',
                    'add=1&category_name='+ name
                    +'&category_desc='+ desc,
                    function (result) {
                        if (result === 'ok') {
                            alert('添加分类成功');
                            location.assign('category_list.php');
                        } else {
                            alert(result);
                        }
                    }
                );
            });
        });
    </script>
</div>
</body>
</html>
