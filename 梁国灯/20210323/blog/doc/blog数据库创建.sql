-- 创建数据库blog
CREATE DATABASE IF NOT EXISTS blog
	DEFAULT CHARACTER SET utf8mb4
	DEFAULT COLLATE utf8mb4_general_ci;
-- 使用数据库
USE blog;
-- 创建分类表
CREATE TABLE IF NOT EXISTS `category`
(
	`category_id` INT UNSIGNED AUTO_INCREMENT COMMENT '分类id',
	`category_name` VARCHAR(20) NOT NULL COMMENT '分类名称',
	`category_desc` VARCHAR(200) NOT NULL COMMENT '分类描述',
	`add_time` INT(11) NOT NULL COMMENT '分类增加时间',
	`update_time` INT(11) NOT NULL COMMENT '分类修改时间',
	PRIMARY KEY (`category_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='分类表';
-- 创建文章表
CREATE TABLE IF NOT EXISTS `article`
(
	`article_id` INT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT '文章id',
	`article_title` VARCHAR(50) NOT NULL COMMENT '文章标题',
	`category_id` INT UNSIGNED NOT NULL COMMENT '文章所属分类',
	`article_author` VARCHAR(20) DEFAULT '佚名' COMMENT '文章作者',
	`article_intro` VARCHAR(100) DEFAULT '这个作者很懒,什么都没有写' COMMENT '文章简介',
	`article_content` VARCHAR(10000) NOT NULL COMMENT '文章内容',
	`add_time` INT(11) UNSIGNED NOT NULL COMMENT '文章增加时间',
	`update_time` INT(11) UNSIGNED NOT NULL COMMENT '文章修改时间',
	PRIMARY KEY (`article_id`),
	FOREIGN KEY (`category_id`) REFERENCES `category`(`category_id`) 
		ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='文章表';
-- 创建管理员表
CREATE TABLE `admin`
(
	`admin_id` INT UNSIGNED AUTO_INCREMENT COMMENT '管理员编号',
	`admin_name` VARCHAR(20) NOT NULL COMMENT '管理员名称',
	`admin_email` VARCHAR(30) NOT NULL COMMENT '管理员邮箱',
	`admin_password` VARCHAR(11) NOT NULL COMMENT '管理员密码',
	`register_time` INT(11) UNSIGNED NOT NULL COMMENT '管理员注册时间',
	PRIMARY KEY (`admin_id`),
	UNIQUE KEY (`admin_name`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';