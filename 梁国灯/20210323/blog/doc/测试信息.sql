-- 查询文章列表信息
SELECT
	article_id,
	article_title,
	category_name,
	article_author,
	article_intro,
	article.add_time,
	article.update_time
	FROM category
	INNER JOIN article ON category.category_id = article.category_id
	ORDER BY article_id DESC;
	
-- 插入分类测试数据
INSERT INTO category(category_name, category_desc, add_time, update_time)
VALUES ('web前端', 'html,css,js,vue等', UNIX_TIMESTAMP(now()), UNIX_TIMESTAMP(now()));

-- 插入文章测试数据
INSERT INTO article (article_title, article_intro, article_author, category_id, article_content, add_time, update_time)
VALUES ('Vue新手入门指南', 'vue入门指南，适合刚接触vue的用户', '小红', 1, '这是vue的文章内容', UNIX_TIMESTAMP(now()), UNIX_TIMESTAMP(now()));

-- 再次插入分类测试数据
INSERT INTO `category` (category_name, category_desc, update_time, add_time)
VALUES 
	('php', 'php编程语言', UNIX_TIMESTAMP(now()), UNIX_TIMESTAMP(now())),
	('SQL Server', 'MSSQL', UNIX_TIMESTAMP(now()), UNIX_TIMESTAMP(now()));

-- 插入管理员测试数据
INSERT INTO admin (admin_name, admin_email, admin_password, register_time)
VALUES ('小红', '123456789@qq.com', '123456', UNIX_TIMESTAMP(now()));
	