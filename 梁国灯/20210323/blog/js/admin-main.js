$(function() {
    $('#logout').click(function() {
        if (confirm('确定要退出登录吗？')) {
            $.post(
                'logout.php',
                function (result) {
                    if (result === 'ok') {
                        alert('退出登录成功');
                        location.assign('login.php');
                    } else {
                        alert(result);
                    }
                }
            );
        }
    });
    $('#all').click(function() {
        $('.check-box').prop('checked', true);
    });
    $('#del-all').click(function() {
        let num = 0;
        $('.check-box').each(function() {
            if ($(this).prop('checked')) {
                num++;
            }
        });
        if (num === 0) {
            return;
        }
        if (confirm('是否确认删除所选项？')) {
            if ($(this).attr('flag') === '1') {
                // 分类
                categoryDelete();
            } else if ($(this).attr('flag') === '2') {
                // 文章
                articleDelete();
            } else {
                // 其它
                alert('参数错误，请联系管理员');
            }
        }
    });

    function categoryDelete() {
        let arr = [];
        $('.check-box').each(function () {
            if ($(this).prop('checked')) {
                arr.push($(this).val());
            }
        });
        $.post(
            'category_delete.php',
            'del=1&category_id='+ arr.join(','),
            function (result) {
                if (result === 'ok') {
                    alert('删除成功');
                    location.assign('category_list.php');
                } else {
                    alert(result);
                }
            }
        );
    }
    function articleDelete() {
        let arr = [];
        $('.check-box').each(function () {
            if ($(this).prop('checked')) {
                arr.push($(this).val());
            }
        });
        $.post(
            'article_delete.php',
            'del=1&article_id='+ arr.join(','),
            function (result) {
                if (result === 'ok') {
                    alert('删除成功');
                    location.assign('article_list.php');
                } else {
                    alert(result);
                }
            }
        );
    }
});