<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
date_default_timezone_set("PRC");
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root', '123456');
$sql = "SELECT * FROM category ORDER BY category_id DESC;";
$db->query("set names utf8");
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>分类列表</title>
    <link type="text/css" rel="stylesheet" href="../css/admin-main.css">
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div id="container">
    <div id="header">
        <h2>博客管理系统</h2>
        <div class="info">
            欢迎你：<span>admin</span>
            <a href="logout.php" id="logout">退出登录</a>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php" class="action">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
<!--            <li><a href="javascript:void(0);">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <div class="breadcrumbs-nav">
            <a href="article_list.php">首页</a> &gt;
            <a href="category_list.php">分类管理</a> &gt;
            <a href="javascript:void(0);">分类列表</a>
        </div>
        <div class="admin-menu">
            <button id="all">全选</button>
            <a href="javascript:void(0);" id="del-all" flag="1">删除已选择的分类</a>
            <a href="category_add.php" id="add">增加分类</a>
        </div>
        <table>
            <tr>
                <th></th>
                <th>分类编号</th>
                <th>分类名称</th>
                <td>分类描述</td>
                <th>增加时间</th>
                <th>修改时间</th>
                <th>操作</th>
            </tr>
            <?php foreach ($categoryList as $value): ?>
                <tr>
                    <td><input type="checkbox" class="check-box" value="<?php echo $value['category_id']; ?>"/></td>
                    <td><?php echo $value['category_id']; ?></td>
                    <td><?php echo $value['category_name']; ?></td>
                    <td><?php echo $value['category_desc']; ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $value['add_time']); ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $value['update_time']); ?></td>
                    <td>
                        <a href="category_edit.php?id=<?php echo $value['category_id']; ?>">编辑</a>
                        <a href="javascript:void(0);" category-id="<?php echo $value['category_id'] ?>" class="delete">删除</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<script src="../js/admin-main.js"></script>
<script>
    $(function () {
        $('.delete').click(function () {
            if (confirm('是否确认删除分类?')) {
                $.post(
                    'category_delete.php',
                    'del=1&category_id=' + $(this).attr('category-id'),
                    function (result) {
                        if (result === 'ok') {
                            alert('删除分类成功');
                            location.assign('category_list.php');
                        } else {
                            alert(result);
                        }
                    }
                );
            }
        });

    })
</script>
</body>
</html>
