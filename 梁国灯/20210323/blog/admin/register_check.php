<?php
if ($_POST['re'] !== '1') {
    header('location: register.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root', '123456');
$db->query("set names utf8");

$adminName = $_POST['admin_name'];
$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$confirmPassword = $_POST['confirm_password'];

// 检查数据库中是否已存在用户名
$sql = "SELECT * FROM admin WHERE admin_name = '{$adminName}';";
$result = $db->query($sql);
$nameFlag = $result->fetch(PDO::FETCH_ASSOC);
if ($nameFlag) {
    echo "该用户名已存在，请更换后再试";
    exit();
}

// 检查数据库中是否已存在邮箱
$sql = "SELECT * FROM admin WHERE admin_email = '{$adminEmail}'";
$result = $db->query($sql);
$emailFlag = $result->fetch(PDO::FETCH_ASSOC);
if ($emailFlag) {
    echo "该邮箱已存在，请更换后再试";
    exit();
}

// 检查两次密码是否一致
if ($adminPassword !== $confirmPassword) {
    echo "两次输入的密码不一致，请检查后再试";
    exit();
}

$sql = "INSERT INTO admin (admin_name, admin_email, admin_password, register_time)
          VALUES ('{$adminName}', '{$adminEmail}', '{$confirmPassword}', ". time() .");";
$result = $db->exec($sql);
if ($result) {
    echo 'ok';
} else {
    echo "发生错误，错误信息：". $db->errorInfo()[2] ."，请联系管理员";
}