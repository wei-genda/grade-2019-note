<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>注册</title>
    <link type="text/css" rel="stylesheet" href="../css/login.css"/>
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div class="content">
    <span style="font-size: 20px;">博客系统</span>
    <form action="javascript:void(0);" method="post">
        <ul class="info">
            <li>请输入相关信息</li>
            <li><input type="text" required="required" id="admin-name"/><span>用户名</span></li>
            <li><input type="text" required="required" id="admin-email"/><span>邮箱</span></li>
            <li><input type="password" required="required" id="admin-password"/><span>密码</span></li>
            <li><input type="password" required="required" id="confirm-password"/><span>确认密码</span></li>
            <li><button type="button" id="register">注册</button></li>
        </ul>
    </form>
    已有账号？<a href="login.php">登录</a>
</div>
<script>
    $(function() {
        $('#register').click(function() {
            let name = $("#admin-name").val();
            let email = $('#admin-email').val();
            let password = $('#admin-password').val();
            let confirmPassword = $('#confirm-password').val();
            $.post(
                'register_check.php',
                're=1&admin_name='+ name
                    +'&admin_email='+ email
                    +'&admin_password='+ password
                    +'&confirm_password='+ confirmPassword,
                function(result) {
                    if (result === 'ok') {
                        alert('注册成功');
                        location.assign('login.php');
                    } else {
                        alert(result);
                    }
                }
            );
        });
    });
</script>
</body>
</html>