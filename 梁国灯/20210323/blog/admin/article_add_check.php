<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
if ($_POST['add'] !== '1') {
    header('location: article_add.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root','123456');
$db->query("set names utf8");

$articleTitle = $_POST['article_title'];
$articleIntro = $_POST['article_intro'];
$articleAuthor = $_POST['article_author'];
$categoryId = $_POST['category_id'];
$articleContent = $_POST['article_content'];

// 检查数据库中是否有相同标题的文章
$sql = "SELECT COUNT(*) AS article_num FROM article WHERE article_title = '{$articleTitle}';";
$result  = $db->query($sql);
$articleNum = (int)$result->fetch(PDO::FETCH_ASSOC)['article_num'];
if ($articleNum > 1) {
    echo "列表里已存在该标题的文章，请更换后再试";
    exit();
}
$sql = "INSERT INTO article (article_title, article_intro, article_author, category_id, article_content, add_time, update_time)
          VALUES ('{$articleTitle}', '{$articleIntro}', '{$articleAuthor}', '{$categoryId}', '{$articleContent}', ". time() .", ". time() ." );";
$result = $db->exec($sql);
if ($result) {
    echo 'ok';
} else {
    echo "发生了错误，错误信息：". $db->errorInfo()[2] ."，请联系管理员";
}