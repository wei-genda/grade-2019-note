<?php
if (empty($_COOKIE['admin_name'])) {
    header('location: login.php');
    exit();
}
if (setcookie('admin_name', '', time() - 1, '/')) {
    echo "ok";
} else {
    echo "退出登录失败，请联系管理员";
}