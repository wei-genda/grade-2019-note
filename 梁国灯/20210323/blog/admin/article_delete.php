<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
if ($_POST['del'] !== '1') {
    header('location: category_list.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root','123456');
$db->query("set names utf8");

$articleId = $_POST['article_id'];

$sql = "DELETE FROM article WHERE article_id = {$articleId}";
$result = $db->exec($sql);
if ($result) {
    echo "ok";
} else {
echo "发生了错误，错误信息：". $db->errorInfo()[2] ."，请联系管理员";
}