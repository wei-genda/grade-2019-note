<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
if ($_POST['del'] !== '1') {
    header('location: category_list.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root','123456');
$categoryId = $_POST['category_id'];

// 删除前检查列表下有没有文章
$sql = "SELECT COUNT(*) AS article_num FROM article WHERE category_id = {$categoryId}";
$result = $db->query($sql);
$articleNum = (int)$result->fetch(PDO::FETCH_ASSOC)['article_num'];
if ($articleNum > 0) {
    echo "该分类下还有文章，无法删除。若要删除该分类，请先删除该分类下所有文章";
    exit();
}
$sql = "DELETE FROM category WHERE category_id = {$_POST['category_id']}";
$db->query("set names utf8");
$result = $db->exec($sql);
if ($result) {
    echo "ok";
} else {
    echo $db->errorInfo()[2];
}