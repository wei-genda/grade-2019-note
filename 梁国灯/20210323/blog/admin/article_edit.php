<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
$id = $_GET['id'] ?? '';
if (empty($id)) {
    header('location: category_list.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root','123456');
$db->query("set names utf8");

// 查找文章信息
$sql = "SELECT * FROM article WHERE article_id = {$id}";
$result = $db->query($sql);
$articleInfo = $result->fetch(PDO::FETCH_ASSOC);

// 查找分类列表
$sql = "SELECT * FROM category";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑文章</title>
    <link type="text/css" rel="stylesheet" href="../css/admin-main.css">
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div id="container">
    <div id="header">
        <h2>博客管理系统</h2>
        <div class="info">
            欢迎你：<span>admin</span>
            <a href="logout.php" id="logout">退出登录</a>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php" class="action">文章管理</a></li>
<!--            <li><a href="javascript:void(0);">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <div class="breadcrumbs-nav">
            <a href="article_list.php">首页</a> &gt;
            <a href="article_list.php">文章管理</a> &gt;
            <a href="javascript:void(0);">文章编辑</a>
        </div>
        <form action="javascript:void(0);" method="post">
            <table class="add">
                    <input type="hidden" value="<?php echo $articleInfo['article_id']; ?>" id="article-id" />
                <tr>
                    <td>文章标题：</td>
                    <td><input type="text" value="<?php echo $articleInfo['article_title']; ?>" id="article-title" /></td>
                </tr>
                <tr>
                    <td>文章简介：</td>
                    <td><textarea rows="5" cols="50" id="article-intro"><?php echo $articleInfo['article_intro']; ?></textarea></td>
                </tr>
                <tr>
                    <td>作者：</td>
                    <td><input type="text" value="<?php echo $articleInfo['article_author']; ?>" id="article-author" /></td>
                </tr>
                <tr>
                    <td>文章所在分类：</td>
                    <td>
                        <select id="category-id">
                            <?php foreach($categoryList as $value): ?>
                            <option value="<?php echo $value['category_id']; ?>" <?php echo $articleInfo['category_id'] === $value['category_id'] ? 'selected="selected"' : '' ?>><?php echo $value['category_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>文章内容：</td>
                    <td><textarea rows="15" cols="58" id="article-content"><?php echo $articleInfo['article_content']; ?></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="button" id="submit">提交</button>
                        <button type="reset">重置</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script src="../js/admin-main.js"></script>
<script>
    $(function() {
        $('#submit').click(function() {
            let title = $('#article-title').val();
            let intro = $('#article-intro').val();
            let author = $('#article-author').val();
            let category = $('#category-id').val();
            let content = $('#article-content').val();
            let articleId = $('#article-id').val();
            $.post(
                'article_edit_check.php',
                'edit=1&article_title='+ title
                    +'&article_intro='+ intro
                    +'&article_author='+ author
                    +'&category_id='+ category
                    +'&article_content='+ content
                    +'&article_id='+ articleId,
                function(result) {
                    if (result === 'ok') {
                        alert('修改文章成功');
                        location.assign('article_list.php');
                    } else {
                        console.log(result);
                        alert(result);
                    }
                }
            );
        });
    })
</script>
</body>
</html>
