<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
if ($_POST['add'] !== '1') {
    header('location: category_add.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root','123456');
$db->query("set names utf8");

$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];

// 检查数据库中是否有同名的分类
$sql = "SELECT COUNT(*) AS category_num FROM category WHERE category_name = '{$categoryName}';";
$result = $db->query($sql);
$categoryNum = (int)$result->fetch(PDO::FETCH_ASSOC)['category_num'];
if ($categoryNum > 1) {
    echo '列表中已有同名的分类，请更换后再试';
    exit();
}
$result = $db->exec($sql);
if ($result) {
    echo 'ok';
} else {
    echo "发生了错误，错误信息：". $db->errorInfo()[2] ."，请联系管理员";
}