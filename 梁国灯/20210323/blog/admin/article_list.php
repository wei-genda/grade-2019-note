<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
date_default_timezone_set("PRC");
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root', '123456');
$db->query("set names utf8");
$sql =
    "SELECT
        article_id,
        article_title,
        category_name,
        article_author,
        article_intro,
        article.add_time,
        article.update_time
        FROM article
        INNER JOIN category ON article.category_id = category.category_id
        ORDER BY article_id DESC";
$result = $db->query($sql);
$articleList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台首页</title>
    <link type="text/css" rel="stylesheet" href="../css/admin-main.css">
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div id="container">
    <div id="header">
        <h2>博客管理系统</h2>
        <div class="info">
            欢迎你：<span>admin</span>
            <a href="logout.php" id="logout">退出登录</a>
        </div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php" class="action">文章管理</a></li>
<!--            <li><a href="javascript:void(0);">管理员</a></li>-->
        </ul>
    </div>
    <div id="right">
        <div class="breadcrumbs-nav">
            <a href="article_list.php">首页</a> &gt;
            <a href="article_list.php">文章管理</a> &gt;
            <a href="javascript:void(0);">文章列表</a>
        </div>
        <div class="admin-menu">
            <button id="all">全选</button>
            <a href="javascript:void(0);" id="del-all" flag="2">删除已选择的文章</a>
            <a href="article_add.php" id="add">增加文章</a>
        </div>
        <table>
            <tr>
                <th></th>
                <th>ID</th>
                <th>标题</th>
                <th>分类</th>
                <td>作者</td>
                <th>简介</th>
                <th>发表时间</th>
                <th>修改时间</th>
                <th>操作</th>
            </tr>
            <?php foreach($articleList as $value): ?>
            <tr>
                <td><input type="checkbox" class="check-box" value="<?php echo $value['article_id']; ?>" /></td>
                <td><?php echo $value['article_id']; ?></td>
                <td><?php echo $value['article_title']; ?></td>
                <td><?php echo $value['category_name']; ?></td>
                <td><?php echo $value['article_author']; ?></td>
                <td><?php echo $value['article_intro']; ?></td>
                <td><?php echo date("Y-m-d H:i:s", $value['add_time']); ?></td>
                <td><?php echo date("Y-m-d H:i:s", $value['update_time']); ?></td>
                <td>
                    <a href="article_edit.php?id=<?php echo $value['article_id']; ?>">编辑</a>
                    <a href="javascript:void(0);" article-id="<?php echo $value['article_id']; ?>" class="delete">删除</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<script src="../js/admin-main.js"></script>
<script>
    $(function () {
        $('#logout').click(function() {
            if (confirm('确定要退出登录吗？')) {
                $.post(
                    'logout.php',
                    function (result) {
                        if (result === 'ok') {
                            alert('退出登录成功');
                            location.assign('login.php');
                        } else {
                            alert(result);
                        }
                    }
                );
            }
        });
        $('.delete').click(function () {
            console.log('---');
            if (confirm('是否确认删除文章?')) {
                $.post(
                    'article_delete.php',
                    'del=1&article_id=' + $(this).attr('article-id'),
                    function (result) {
                        if (result === 'ok') {
                            alert('删除文章成功');
                            location.assign('article_list.php');
                        } else {
                            alert(result);
                        }
                    }
                );
            }
        });
    })
</script>
</body>
</html>
