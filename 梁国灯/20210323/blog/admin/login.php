<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link type="text/css" rel="stylesheet" href="../css/login.css"/>
    <script src="../js/jquery-3.5.1.min.js"></script>
</head>
<body>
<div class="content">
    <span style="font-size: 20px;">博客系统</span>
    <form action="javascript:void(0);" method="post">
        <ul class="info login">
            <li>请输入用户名和密码</li>
            <li><input type="text" required="required" id="admin-name"/><span>用户名</span></li>
            <li><input type="password" required="required" id="admin-password"/><span>密码</span></li>
<!--            <li id="checkbox-remember"><input type="checkbox" id="remember"/><label for="remember">记住我</label></li>-->
            <li><button type="button" id="login">登录</button></li>
        </ul>
    </form>
    新用户？<a href="register.php">创建账号</a>
</div>
<script>
    $(function() {
        $('#login').click(function() {
            let name = $('#admin-name').val();
            let password = $('#admin-password').val();
            $.post(
                'login_check.php',
                'login=1&admin_name='+ name
                    +'&admin_password='+ password,
                function (result) {
                    if (result === 'ok') {
                        alert('登录成功');
                        $('form').submit();
                    } else {
                        console.log(result);
                        alert(result);
                    }
                }
            );
        });
        $('form').submit(function() {
            $(this).attr('action', 'article_list.php');
        });
    });
</script>
</body>
</html>