<?php
$adminName = $_COOKIE['admin_name'] ?? '';
if (empty($adminName)) {
    header('location: login.php');
    exit();
}
if ($_POST['edit'] !== '1') {
    header('location: article.php');
    exit();
}
$dsn = "mysql: host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, 'root','123456');
$db->query("set names utf8");

$articleTitle = $_POST['article_title'];
$articleIntro = $_POST['article_intro'];
$articleAuthor = $_POST['article_author'];
$categoryId = $_POST['category_id'];
$articleContent = $_POST['article_content'];
$articleId = $_POST['article_id'];

$sql =
    "UPDATE article
	SET 
		article_title = '{$articleTitle}',
		article_intro = '{$articleIntro}',
		article_author = '{$articleAuthor}',
		category_id = '{$categoryId}',
		article_content = '{$articleContent}',
		update_time = '". time() ."'
	WHERE article_id = {$articleId};";
$result = $db->exec($sql);
if ($result) {
    echo 'ok';
} else {
    echo "发生了错误，错误信息：". $db->errorInfo()[2] ."，请联系管理员";
}