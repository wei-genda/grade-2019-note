-- 博客数据库
CREATE DATABASE blog
	DEFAULT CHARACTER SET utf8mb4
	DEFAULT COLLATE utf8mb4_general_ci;

USE blog;
-- 分类表
CREATE TABLE `category`
(
	category_id INT AUTO_INCREMENT COMMENT '分类id',
	category_name VARCHAR(20) NOT NULL COMMENT '分类名称',
	category_desc VARCHAR(200) NOT NULL COMMENT '分类描述',
	update_time INT(11) NOT NULL COMMENT '修改时间',
	add_time INT(11) NOT NULL COMMENT '增加时间',
	PRIMARY KEY(category_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='分类表';

-- 文章表
CREATE TABLE `article`
(
	article_id INT AUTO_INCREMENT COMMENT '文章id',
	article_title VARCHAR(50) NOT NULL COMMENT '文章标题',
	category_id INT NOT NULL COMMENT '文章所属分类', 
	article_author VARCHAR(20) DEFAULT '佚名',
	article_content VARCHAR(10000) NOT NULL COMMENT '文章内容',
	update_time INT(11) NOT NULL COMMENT '修改时间',
	add_time INT(11) NOT NULL COMMENT '增加时间',
	PRIMARY KEY (article_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='文章表';
