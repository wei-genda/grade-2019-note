/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-24 10:42:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '标题',
  `article_intro` varchar(500) NOT NULL COMMENT '文章简介',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1', 'html', 'html', 'html', '1616489496', '1616468371');
INSERT INTO `article` VALUES ('2', '1', 'HTML基础知识（一）', 'HTML(Hyper Text Markup Language),中文译名为超文本标记语言，它包括一系列标签，通过这些标签可以将网络上的文档格式统一，使分散的Internet资源连接为一个逻辑整体。简单来说，就是我们用它所包含的一系列标签来定义网页的布局结构。\r\n\r\n', 'HTML 元素以开始标签起始，以结束标签终止，元素的内容是开始标签与结束标签之间的内容。在VSCode中，使用标签只需要输入需要使用的标签名，再按Tab键，编辑器会自动补齐开始标签和结束标签的样式。\r\n温馨提示：\r\n1.标签的括号我们用的是尖括号，这也是HTML代码和CSS（层叠样式表）代码的区别。\r\n2.HTML 标签对大小写不敏感，也就是说不管我们用大写还是小写浏览器都能成功识别，但是为了统一标准，最好还是写小写。\r\n', '1616489000', '1616481731');
INSERT INTO `article` VALUES ('5', '1', 'HTML基础知识（二）', 'HTML(Hyper Text Markup Language),中文译名为超文本标记语言，它包括一系列标签，通过这些标签可以将网络上的文档格式统一，使分散的Internet资源连接为一个逻辑整体。简单来说，就是我们用它所包含的一系列标签来定义网页的布局结构。\r\n\r\n', 'HTML 元素以开始标签起始，以结束标签终止，元素的内容是开始标签与结束标签之间的内容。在VSCode中，使用标签只需要输入需要使用的标签名，再按Tab键，编辑器会自动补齐开始标签和结束标签的样式。\r\n温馨提示：\r\n1.标签的括号我们用的是尖括号，这也是HTML代码和CSS（层叠样式表）代码的区别。\r\n2.HTML 标签对大小写不敏感，也就是说不管我们用大写还是小写浏览器都能成功识别，但是为了统一标准，最好还是写小写。\r\n', '1616549890', '1616549851');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'html', 'html', '1616469710', '1616465895');
INSERT INTO `category` VALUES ('4', 'css', '分类', '1616477864', '1616477864');
INSERT INTO `category` VALUES ('10', 'vue', 'vue介绍', '1616548473', '1616548473');
INSERT INTO `category` VALUES ('11', 'js', 'js简介', '1616548484', '1616548484');
INSERT INTO `category` VALUES ('12', 'java', 'java简介', '1616548499', '1616548499');
