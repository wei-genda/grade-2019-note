<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/22
 * Time: 16:32
 */
$categoryName=$_POST['category_name'];
$categoryDesc=$_POST['category_desc'];

if (mb_strlen($categoryName==null)){
    echo '添加失败,分类名称不为空<a href="category_add.php">重新添加</a>';
    exit();
}else if(mb_strlen($categoryDesc==null)){
    echo '添加失败,分类简介不为空<a href="category_add.php">重新添加</a>';
    exit();
}

date_default_timezone_set("PRC");
$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$sql="SELECT * from category order by category_id desc";
$result=$db->query($sql);
$categoryList=$result->fetchAll(PDO::FETCH_ASSOC);
foreach ($categoryList as $item){
    if ($item['category_name']==$categoryName){
        echo '分类名称已存在<a href="category_add.php">重新添加</a>';
        exit();
    }
}

$addTime=time();
$updateTime=$addTime;

$sql="insert into category(category_name,category_desc,add_time,update_time) value 
('$categoryName','$categoryDesc','$addTime','$updateTime')";

$result=$db->exec($sql);

if ($result){
    echo "插入成功.<a href='category_list.php'>返回列表页面</a>";
    exit();
}else{
    echo "插入失败,信息错误:".$db->errorInfo()[2];
}