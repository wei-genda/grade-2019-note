<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/22
 * Time: 16:32
 */
$articleTitle=$_POST['article_title'];
$categoryId=$_POST['category_id'];
$articleIntro=$_POST['article_intro'];
$content=$_POST['content'];

if (mb_strlen($articleTitle==null)){
    echo '添加失败,文章标题不为空<a href="article_add.php">重新添加</a>';
    exit();
}else if (mb_strlen($articleIntro==null)){
    echo '添加失败,文章简介不为空<a href="article_add.php">重新添加</a>';
    exit();
}else if (mb_strlen($content==null)) {
    echo '添加失败,文章内容不为空<a href="article_add.php">重新添加</a>';
    exit();
}

date_default_timezone_set("PRC");

$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$addTime=time();
$updateTime=$addTime;

$sql="insert into article(article_title,category_id, article_intro,content,add_time,update_time) value 
('$articleTitle','$categoryId','$articleIntro','$content','$addTime','$updateTime')";

$result=$db->exec($sql);

if ($result){
    echo "插入成功.<a href='article.php'>返回列表页面</a>";
    exit();
}else{
    echo "插入失败,信息错误:".$db->errorInfo()[2];
}