<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 10:58
 */
//$dsn="mysql:Server=root;Database=blog";
//$db=new PDO($dsn,"sa","123456");
//$sql="SELECT * from category order by category_id ";
//$statement=$db->query($sql);
//$result=$statement->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("PRC");
$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$sql="SELECT * from category order by category_id desc";
$result=$db->query($sql);
$categoryList=$result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" href="css/top.css" type="text/css" />
    <link rel="stylesheet" href="css/item.css" type="text/css" />
    <link rel="stylesheet" href="css/center.css" type="text/css" />
    <script src="js/jquery.js"></script>
</head>
<div id="content">
    <div id="top">
        <li>欢迎你：admin &nbsp; 退出登录</li>
        <div id="topone">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="center">
        <div id="centerone">
            <ul>
                <li><a href="category_list.php">分类管理</a></li>
                <li><a href="article.php">文章管理</a></li>
                <li><a href="#">管理员</a></li>
            </ul>
        </div>
        <div id="centertwo">
            <ul>
                <li><a href="#">首页</a>>&nbsp;<a href="#">分类管理</a>>&nbsp;<a href="#">分类列表</a></li>
                <li><input type="button" id="all" value="全选" class="butstyle" /><span><a id="multi_delete" href="javascript:void(0);">删除选中任务</a></span></li>
                <li class="a"><a href="category_add.php">增加分类</a></li>
            </ul>
            <form action="category_multi_delete.php" id="multi_delete_form">
                <table style="width:100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>分类名称</th>
                        <th>增加时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $row ):?>
                    <tr>
                        <td><input type="checkbox" class="category_checkbox"  name="categoryId[]" value="<?php echo $row['category_id']?>" /></td>
                        <td><?php echo $row['category_id'];?></td>
                        <td><?php echo $row['category_name'];?></td>
                        <td><?php echo date("Y-m-d H:i:s",$row['add_time']);?></td>
                        <td><?php echo date("Y-m-d H:i:s",$row['update_time']);?></td>
                        <td id="b">
                            <a href="category_edit.php?category_id=<?php echo $row['category_id']?>">编辑</a>
                            <a class="delete" href="category_delete.php?category_id=<?php echo $row['category_id']?>">删除</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </form>
        </div>
    </div>

</div>
<script src="js/main.js"></script>
<body>
</body>
</html>

