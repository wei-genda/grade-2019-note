<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 10:58
 */
date_default_timezone_set("PRC");
$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");
$sql="SELECT * from category order by category_id desc";
$result=$db->query($sql);
$categoryList=$result->fetchAll(PDO::FETCH_ASSOC);
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" href="css/top.css" type="text/css" />
    <link rel="stylesheet" href="css/item.css" type="text/css" />
    <link rel="stylesheet" href="css/center.css" type="text/css" />
</head>
<div id="content">
    <div id="top">
        <li>欢迎你：admin &nbsp; 退出登录</li>
        <div id="topone">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="center">
        <div id="centerone">
            <ul>
                <li><a href="category_list.php">分类管理</a></li>
                <li><a href="article.php">文章管理</a></li>
                <li><a href="#">管理员</a></li>
            </ul>
        </div>
        <div id="centertwo">
            <ul>
                <li><a href="category_list.php">首页</a>>&nbsp;<a href="article.php">文章列表</a>>&nbsp;<a href="article_add.php">增加文章</a></li>
            </ul>
            <form action="article_add_save.php" method="post">
                <table   class="update" cellspacing="0" style="width:100%">
                    <tr>
                        <td class="c">文章标题：</td>
                    <td><input type="text" name="article_title"/></td>
                    </tr>
                    <tr>
                        <td class="c">文章分类：</td>
                        <td>
                            <select name="category_id">
                                <?php foreach ($categoryList as $row): ?>
                                <option value="<?php echo $row['category_id'] ?>">
                                    <?php echo $row['category_name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="c">文章简介：</td>
                        <td><textarea cols="60" rows="15" name="article_intro"></textarea></td>
                    </tr>
                    <tr>
                        <td class="c">文章内容：</td>
                        <td><textarea cols="60" rows="15" name="content"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" value="提交" class="butstyle" />
                            <input type="reset" value="重置" class="butstyle" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div>

</div>
<body>
</body>
</html>


