<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/19
 * Time: 10:58
 */
date_default_timezone_set("PRC");
$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");
$sql="SELECT * from article order by article_id desc";
$result=$db->query($sql);
$articleList=$result->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" href="css/top.css" type="text/css" />
    <link rel="stylesheet" href="css/item.css" type="text/css" />
    <link rel="stylesheet" href="css/center.css" type="text/css" />
    <script src="js/jquery.js"></script>
</head>
<div id="content">
    <div id="top">
        <li>欢迎你：admin &nbsp; 退出登录</li>
        <div id="topone">
            <h2>博客管理系统</h2>
        </div>
    </div>
    <div id="center">
        <div id="centerone">
            <ul>
                <li><a href="category_list.php">分类管理</a></li>
                <li><a href="article.php">文章管理</a></li>
                <li><a href="#">管理员</a></li>
            </ul>
        </div>
        <div id="centertwo">
            <ul>
                <li><a href="#">首页</a>>&nbsp;<a href="#">文章管理</a>>&nbsp;<a href="#">文章列表</a></li>
                <li><input type="button" id="all" value="全选" class="butstyle" /><span><a id="multi_delete" href="javascript:void(0)">删除选中任务</a></span></li>
                <li class="a"><a href="article_add.php">增加文章</a></li>
            </ul>
            <form  action="article_multi_delete.php" id="multi_delete_form">
                <table style="width:100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>文章标题</th>
                        <th>所属分类</th>
                        <th>文章简介</th>
                        <th>添加时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($articleList as $row ):?>
                        <tr>
                            <td><input type="checkbox" class="article_checkbox"  name="articleId[]" value="<?php echo $row['article_id'];?>" /></td>
                            <td><?php echo $row['article_id'];?></td>
                            <td><?php echo $row['article_title'];?></td>
                            <td><?php
                                $dsn="mysql:host=127.0.0.1;dbname=blog";
                                $db=new PDO($dsn,"root","123456");
                                $sql="SELECT * from category  where category_id='{$row['category_id']}'";
                                $result=$db->query($sql);
                                $articleList=$result->fetch(PDO::FETCH_ASSOC);
                                echo $articleList['category_name']
                                ;?></td>
                            <td><?php echo $row['article_intro'];?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['add_time']);?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['update_time']);?></td>
                            <td id="b">
                                <a href="article_edit.php?article_id=<?php echo $row['article_id']?>">编辑</a>
                                <a class="delete" href="article_delete.php?article_id=<?php echo $row['article_id']?>">删除</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </form>
        </div>
    </div>

</div>
<script src="js/main.js"></script>
<body>
</body>
</html>
