-- 查询商品表所有数据。
select * FROM item;

-- 查询价格小于2000的商品。
select * from item where item_price < 2000;

-- 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';

-- 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3;

-- 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 offset 3;

-- 查询所有商品，按照价格进行升序排序。
select * from item order by item_price aSC;

-- 查询价格小于2000的商品总数。
select count(*) as '商品总数' from item where item_price < 2000;

-- 查询购物车中所有数据。
select * from cart;

-- 查询每个用户加入购物车的商品总数。
SELECT user_id as '用户',sum(item_num) as '商品总数' from cart group by user_id;

-- 查询加入购物车的商品总数小于2的用户。


SELECT
	user_id AS '用户',
	sum(item_num) AS '商品总数'
FROM
	cart
GROUP BY
	user_id
HAVING
	sum(item_num) < 2;



-- 查询有加入购物车的商品。
select distinct item.* from cart inner join item on cart.item_id=item.item_id;

-- 查询没有加入购物车的商品。
select item.* from item left join cart on item.item_id=cart.item_id where cart.cart_id is null;

-- 1.用户4将将商品1从购物车中移除。
delete from cart where user_id=4 and item_id=1;

-- 2.用户3清空购物车。
delete from cart where user_id=3;

-- 3.移除购物车中的所有数据：
-- 使用delete进行删除
 delete from cart;

-- 使用truncate进行删除
truncate table cart;

-- 对比delete和truncate的差别
-- 区别：使用delete不会影响自增列的变化，通常用来删除部分，且删除输的慢；
--     使用truncate直接清空表数据，自增列同样清空，通常用来删除表中全部数据，且删除速度较快；