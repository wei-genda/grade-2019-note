<?php

$categoryid = $_GET["categoryid"];

//定位东八区的时间
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");

$db->exec("set names utf8mb4");

$sql="SELECT * FROM category WHERE category_id = '$categoryid'";
$result = $db->query($sql);
$cateforyList = $result->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>新闻增加页面</title>
    <link rel="stylesheet" type="text/css" href="../ui/增加内容页面/css/base.css">
    <link rel="stylesheer" type="text/css" href="../ui/增加内容页面/css/top.css">
    <link rel="stylesheet" type="text/css" href="../ui/增加内容页面/css/left.css">
    <link rel="stylesheet" type="text/css" href="../ui/增加内容页面/css/right.css">
</head>
<body>
<div id="cont">
    <div id="top">
        <div id="top_a">
            <h1>博客管理系统</h1>
        </div>
        <div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category.php">分类管理</a></li>
            <li><a href="#">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right_a">
            <a href="#">首页</a>&gt;
            <a href="article.php">文章列表</a>&gt;
            <a href="category.php">分类列表</a>
        </div>
        <div id="right_b">
            <form action="category_update_save.php" method="post">
                <table border="">
                    <tr>
                        <td  width="190px">分类id ：</td>
                        <td><input type="text" id="a" name="category_id" value="<?php echo $cateforyList['category_id']?>" readonly="readonly"/></td>
                    </tr>
                    <tr>
                        <td>分类名称：</td>
                        <td><input type="text" id="b" name="category_name" value="<?php echo $cateforyList['category_name']?>"/></td>
                    </tr>
                    <tr>
                        <td>分类内容：</td>
                        <td><textarea rows="" cols="" id="d" name="category_desc"><?php echo $cateforyList['category_desc']?></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="提交" class="but"  />
                            <input type="reset" value="重置" class="but" />
                        </td>
                    </tr>

                </table>
            </form>
        </div>

    </div>
</div>
</body>
</html>

