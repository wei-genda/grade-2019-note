<?php

$articleid = $_GET["articleid"];


//定位东八区的时间
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");
//设计字符集
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM article where article_id = '$articleid'";
$result = $db->query($sql);
$cateforyList = $result->fetch(PDO::FETCH_ASSOC);
//$categoryid = $cateforyList["category_id"];

$sqla = "SELECT * FROM category ORDER BY category_id DESC";
$word = $db->query($sqla);
$catefory = $word->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>新闻增加页面</title>
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/base.css">
    <link rel="stylesheer" type="text/css" href="增加内容页面/css/top.css">
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/left.css">
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/right.css">
</head>
<body>
<div id="cont">
    <div id="top">
        <div id="top_a">
            <h1>博客管理系统</h1>
        </div>
        <div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right_a">
            <a href="#">首页</a>&gt;
            <a href="#">文章管理</a>&gt;
            <a href="category.php">分类列表</a>
        </div>
        <div id="right_b">
            <form action="article_update_save.php" method="post">
                <table border="">
                    <tr>
                        <td  width="190px">文章id ：</td>
                        <td><input type="text" id="a" name="article_id" value="<?php echo $cateforyList['article_id']?>" readonly="readonly" /></td>
                    </tr>
                    <tr>
                        <td>文章标题：</td>
                        <td><input type="text" id="b" name="title" value="<?php echo $cateforyList['article_title']?>"/></td>
                    </tr>
                    <tr>
                        <td>分类名称：</td>
                        <td><select id="c" name="category_id">
                                <?php foreach ($catefory as $row): ?>
                                <option value="<?php echo $row['category_id']?>" <?php echo $cateforyList['category_id'] == $row['category_id'] ? 'selected="selected"' : '' ?>>
                                    <?php echo $row['category_name'] ?></option>
                                <?php endforeach; ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>文章内容：</td>
                        <td><textarea rows="" cols="" id="d" name="content"><?php echo $cateforyList['content']?></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="提交" class="but" />
                            <input type="reset" value="重置" class="but" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
</body>
</html>

