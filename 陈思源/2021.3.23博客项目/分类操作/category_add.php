<?php
/**
 * 增加内容页面
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>新闻增加页面</title>
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/base.css">
    <link rel="stylesheer" type="text/css" href="增加内容页面/css/top.css">
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/left.css">
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/right.css">
</head>
<body>
<div id="cont">
    <div id="top">
        <div id="top_a">
            <h1>博客管理系统</h1>
        </div>
        <div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category.php">分类管理</a></li>
            <li><a href="#">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right_a">
            <a href="#">首页</a>&gt;
            <a href="#">文章管理</a>&gt;
            <a href="category.php">分类列表</a>
        </div>
        <div id="right_b">
            <form action="category_add_save.php" method="post">
                <table border="">

                    <tr>
                        <td>分类名称：</td>
                        <td><input type="text" id="b" name="name"/></td>
                    </tr>

                    <tr>
                        <td>分类内容</td>
                        <td><textarea  id="d" name="text"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="提交" class="but" />
                            <input type="reset" value="重置" class="but" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
</body>
</html>

