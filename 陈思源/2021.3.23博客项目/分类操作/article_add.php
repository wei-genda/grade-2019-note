<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");
//设计字符集
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM category ORDER BY category_id DESC";
$result = $db->query($sql);
$cateforyList = $result->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>文章增加页面</title>
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/base.css">
    <link rel="stylesheer" type="text/css" href="增加内容页面/css/top.css">
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/left.css">
    <link rel="stylesheet" type="text/css" href="增加内容页面/css/right.css">
</head>
<body>
<div id="cont">
    <div id="top">
        <div id="top_a">
            <h1>博客管理系统</h1>
        </div>
        <div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="#">分类管理</a></li>
            <li><a href="#">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right_a">
            <a href="#">首页</a>&gt;
            <a href="category.php">分类列表</a>&gt;
            <a href="article.php">文章列表</a>
        </div>
        <div id="right_b">
            <form action="article_add_save.php" method="post">
                <table border="">

                    <tr>
                        <td>文章标题：</td>
                        <td><input type="text" id="b" name="title"/></td>
                    </tr>
                    <tr>
                        <td>文章分类：</td>

                        <td><select id="c" name="category_id">
                            <?php foreach($cateforyList as $row): ?>
                                <option value="<?php echo $row['category_id']?>"><?php echo $row['category_name'] ?></option>
                                <?php endforeach; ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>文章内容：</td>
                        <td><textarea rows="" cols="" id="d" name="content"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="提交" class="but" />
                            <input type="reset" value="重置" class="but" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
</body>
</html>

