<?php
/**
 * 分类首页
*/
//定位东八区的时间
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");
//设计字符集
$db->exec("set names utf8mb4");

$sql = "SELECT * FROM category ORDER BY category_id DESC";
$result = $db->query($sql);
$cateforyList = $result->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台首页</title>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <link type="text/css" rel="stylesheet" href="css/top.css" />
    <link type="text/css" rel="stylesheet" href="css/left.css" />
    <link type="text/css" rel="stylesheet" href="css/right.css" />
</head>
<body>
<div id="cont">
    <div id="top">
        <div id="top_a">
            <h1>博客管理系统</h1>
        </div>
        <div id="top_b">欢迎你: admin <a href="#">推出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category.php">分类列表</a></li>
            <li><a href="article.php">文章列表</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right_a">
            <a href="#">首页</a>&gt;
            <a href="article.php">文章列表</a>&gt;
            <a href="category.php">分类列表</a>
        </div>
        <div id="right_b">
            <button id="submin">全选</button>
            <a href="#">删除选中分类</a>
            <a id="a" href="category_add.php">增加分类</a>
        </div>
        <div id="right_c">
            <form action="category_add.php">
                    <table border="">
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>分类名称</th>
                            <th>分类描述</th>
                            <th>发表时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        <?php foreach($cateforyList as $row) : ?>
                        <tr>
                            <td><input type="checkbox" class="checkbox"></td>
                            <td><?php echo $row["category_id"]; ?></td>
                            <td><?php echo $row["category_name"]; ?></td>
                            <td><?php echo $row["category_desc"]; ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row["add_time"]) ; ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row["update_time"]) ; ?></td>
                            <td><a href="category_update.php?categoryid=<?php echo$row["category_id"]; ?>">编辑</a>
                                <a href="category_delete.php?categoryid=<?php echo$row["category_id"]; ?>">删除</a>
                            </td>

                        </tr>
                        <?php endforeach; ?>
                    </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>
