<?php

date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");

$db->exec("set names utf8mb4");

$sql = "select * from article order by article_id desc";
$result = $db->query($sql);
$articleList = $result->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客管理系统</title>
    <link href="css/main1.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
    <div id="heard">
        <h1>博客管理系统</h1>
        <div id="list">
            欢迎你：admin&nbsp;<a href="#">退出登录</a>
        </div>
    </div>

    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>

    <div id="right">
        <div id="bredcrumb">
            <a href="article_list.php">首页</a>&gt;
            <a href="article_list.php">文章管理</a>&gt;
            <a href="article_list.php">文章列表</a>
        </div>

        <div id="table-menu">
            <button class="but">全选</button>
            <a href="#">删除选中任务</a>
            <a href="article_add.php" id="add">增加文章</a>
        </div>

        <div id="table">
            <table border="" cellspacing="" cellpadding="">
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>标题</th>
                    <th>分类</th>
                    <th>简介</th>
                    <th>发表时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($articleList as $row): ?>
                    <tr>
                        <td><input type="checkbox" value="" class="buts" /></td>
                        <td><?php echo $row['article_id']; ?></td>
                        <td><?php echo $row['article_title']; ?></td>
                        <td>
                            <?php
                            $sql = "select * from category where category_id='{$row['category_id']}'";
                            $result = $db->query($sql);
                            $category = $result->fetch(PDO::FETCH_ASSOC);
                            echo $category['category_name'];
                            ?>
                        </td>
                        <td><?php echo $row['intro']; ?></td>
                        <td><?php echo date("Y-m-d H:i:s",$row['add_time']); ?></td>
                        <td><?php echo date("Y-m-d H:i:s",$row['update_time']); ?></td>
                        <td><a href="article_edit.php?article_id=<?php echo $row['article_id']; ?>">编辑</a>
                            <a href="article_delete.php?article_id=<?php echo $row['article_id']; ?>">删除</a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
</body>
</html>

