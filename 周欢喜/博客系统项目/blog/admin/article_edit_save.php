<?php

/**
 * 文章编辑保存功能
 */

//获取到文章数据
$articleId = $_POST['article_id'];
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$intro = $_POST['intro'];
$content = $_POST['content'];

//保存到数据库
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$updateTime = time();

$sql = "update article set article_title='$articleTitle',category_id='$categoryId',
        intro='$intro',content='$content',update_time='$updateTime'
        where article_id='$articleId'";

$result = $db->exec($sql);

if ($result) {
    echo "修改成功。<a href='article_list.php'>返回列表页</a>";
    exit();
} else {
    echo "修改失败，错误信息：" . $db->errorInfo()[2].",请联系管理员：1316409499@qq.com";
}