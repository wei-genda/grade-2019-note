<?php
/**
 * 分类增加功能
 */

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客管理系统</title>
    <link href="css/main1.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
    <div id="heard">
        <h1>博客管理系统</h1>
        <div id="list">
            欢迎你：admin&nbsp;<a href="#">退出登录</a>
        </div>
    </div>

    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>

    <div id="right">
        <div id="bredcrumb">
            <a href="article_list.php">首页</a>&gt;
            <a href="category_list.php">分类管理</a>&gt;
            <a href="category_add.php">增加分类</a>
        </div>

        <div id="table-list">
            <form action="category_add_save.php" method="post">
                <table border="" cellspacing="" cellpadding="">
                    <tr>
                        <td class="one">分类名称：</td>
                        <td>
                            <input type="text" value="" class="two" name="category_name" />
                        </td>
                    </tr>
                    <tr>
                        <td class="one">分类描述：</td>
                        <td>
                            <textarea rows="13" cols="60" class="textnet" name="category_desc"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="b2" value="提交" class="but"/>
                            <input type="reset" name="b3" value="重置" class="but"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>

