/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-24 11:08:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(100) NOT NULL COMMENT '标题',
  `intro` varchar(500) NOT NULL COMMENT '简介',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('4', '10', '你好李焕英', '', '', '1616554909', '1616554909');
INSERT INTO `article` VALUES ('5', '9', '阿凡达', '蓝色的人\r\n', '好看', '1616555169', '1616554920');
INSERT INTO `article` VALUES ('6', '13', '欢喜就好', '一首歌曲', '开心欢快的音乐\r\n', '1616555004', '1616555004');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(1000) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('8', 'html', '', '1616554677', '1616554677');
INSERT INTO `category` VALUES ('9', 'css', '1', '1616554796', '1616554796');
INSERT INTO `category` VALUES ('10', 'php', '2\r\n', '1616554881', '1616554881');
INSERT INTO `category` VALUES ('11', 'html&css', '2', '1616554935', '1616554935');
INSERT INTO `category` VALUES ('12', 'html&php', '1', '1616554951', '1616554951');
INSERT INTO `category` VALUES ('13', 'php&css', '2', '1616554961', '1616554961');
