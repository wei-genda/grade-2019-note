CREATE DATABASE IF NOT EXISTS blog
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

use blog;

CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='分类表';


CREATE TABLE `article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '标题',
  `intro` longtext NOT NULL COMMENT '文章内容',
  `content` longtext NOT NULL COMMENT '文章简介',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 comment='文章表';

insert into category (category_name, category_desc, add_time, update_time)
 values ('java', 'Java学习与练习', '111111111' ,'111111111')

insert into article (category_id,article_title,intro, content, add_time, update_time)
 values ('1', 'Java学习与练习','Java的学习入门是比较简单的', 'java是一门很不错的编程语言' ,'111111111','111111111')