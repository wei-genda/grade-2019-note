<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>博客管理系统后台管理员登录</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="login_in.php">
        <table class="update login">
            <caption>
                <h3>管理员登录</h3>
            </caption>
            <tr>
                <td>用户名：</td>
                <td><input type="text" name="Admin_Account"/></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><input type="password" name="Admin_Password"/></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="登录" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

