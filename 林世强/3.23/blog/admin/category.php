<?php
//授权认证登录
//if (empty($_COOKIE['AdminAccount'])){
//    echo "请先登录<br>";
//    echo "<a href='login.php'>进入登陆界面</a>";
//    exit();
//}

date_default_timezone_set("PRC");

$dsn = "mysql:Server=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql="select * from category order by category_id asc";
$result=$db->query($sql);
$categoryList=$result->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客管理系统后台分类列表首页</title>
    <link href="css/Untitled.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
    <div id="toubu">
        <table>
            <tr>
                <td style="background-color:#06F;text-align:left; color:#FFF;width:300px;height:70px ; line-height:70px; font-size:28px; padding-left:15px;">博客管理系统</td>
                <td style="background-color:#09F;color:#fff;width:700px;text-align:right; line-height:70px; padding-right:15px;">
                    欢迎你：<a href="#"><!--<?php echo $_COOKIE['AdminAccount'];?>-->admin</a><a href="login_out.php">&nbsp;&nbsp;&nbsp;退出登录</a></td>

            </tr>
        </table>
    </div>
    <div id="left">
        <table>
            <tr>
                <td style=" border-right:1px #000 solid; width:310px;"><ul>
                        <li><a href="category.php">&nbsp;分类管理</a></li>
                        <li><a href="article.php">&nbsp;新闻管理</a></li>
                        <li><a href="#">&nbsp;管理员</a></li>
                    </ul></td>
            </tr>
        </table>
    </div>
    <div id="middle">
        <ul>
            <li><a href="#">&nbsp;&nbsp;&nbsp;首页&nbsp;</a></li>
            <li>&nbsp;&gt;&nbsp;</li>
            <li><a href="#">&nbsp;分类管理&nbsp;</a></li>
            <li>&nbsp;&gt;&nbsp;</li>
            <li><a href="#">&nbsp;分类列表&nbsp;</a></li>
        </ul>
    </div>
    <div id="middle2">
        <ul>
            <li>
                <input type="button" style="border: 0;" value="全选"/>
            </li>
            <li><a href="#">&nbsp;&nbsp;&nbsp;删除选中分类&nbsp;</a></li>
            <div id="middle21">
                <li><a href="category_add.php" id="category_add">增加分类</a></li>
            </div>
        </ul>
    </div>
    <div id="middle3">
        <table border="1" style="font-size:14px; border-collapse:collapse; text-align:center;width: 688px;">
            <tr>
                <td>&nbsp;</td>
                <td>分类ID</td>
                <td>分类名称</td>
                <td>增加时间</td>
                <td>修改时间</td>
                <td>操作</td>
            </tr>

            <?php foreach ($categoryList as $row): ?>
            <tr>
                <td><input type="checkbox" /></td>
                <td><?php echo $row['category_id']?></td>
                <td><?php echo $row['category_name']?></td>
                <td><?php echo date("y-m-d h:i:s",$row['add_time'])?></td>
                <td><?php echo date("y-m-d h:i:s",$row['update_time'])?></td>
                <td><a href="category_edit.php?category_id=<?php echo $row['category_id']?>" style="text-decoration:none;">编辑</a>
                    <a href="category_delete.php?category_id=<?php echo $row['category_id']?>" style="text-decoration:none;">删除</a></td>
            </tr>
            <?php endforeach;?>

        </table>
    </div>
</div>
</body>
</html>

