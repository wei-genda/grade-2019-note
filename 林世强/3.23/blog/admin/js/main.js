$(".delete").click(function () {
    if (window.confirm("是否确认删除")) {
        return true;
    }
    return false;
});

$("#multi_delete").click(function () {
    if (window.confirm("是否确认批量删除")) {
        $("#list-form").submit();
    }
});

$("#all").click(function () {
    $(".task-checkbox").prop("checked", true);
});