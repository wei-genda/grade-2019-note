<?php
$articleId=$_GET['article_id'];
date_default_timezone_set("PRC");

$dsn = "mysql:Server=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql="select * from article where article_id = '$articleId'";
$result=$db->query($sql);
$article=$result->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客管理系统文章编辑页面</title>
    <link href="css/Untitled-1.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
    <div id="toubu">
        <table>
            <tr>
                <td style="background-color:#06F;text-align:left; color:#FFF;width:300px;height:70px ; line-height:70px; font-size:28px; padding-left:15px;">博客管理系统</td>
                <td style="background-color:#09F;color:#fff;width:700px;text-align:right; line-height:70px; padding-right:15px;">欢迎你：<a href="#">admin</a> <a href="#">&nbsp;&nbsp;&nbsp;退出登录</a></td>
            </tr>
        </table>
    </div>
    <div id="left">
        <table>
            <tr>
                <td style=" border-right:1px #000 solid; width:310px;"><ul>
                        <li><a href="category.php">&nbsp;分类管理</a></li>
                        <li><a href="article.php">&nbsp;新闻管理</a></li>
                        <li><a href="#">&nbsp;管理员</a></li>
                    </ul></td>
            </tr>
        </table>
    </div>
    <div id="middle">
        <ul>
            <li><a href="#">&nbsp;&nbsp;&nbsp;首页&nbsp;</a></li>
            <li>&nbsp;&gt;&nbsp;</li>
            <li><a href="#">&nbsp;文章管理&nbsp;</a></li>
            <li>&nbsp;&gt;&nbsp;</li>
            <li><a href="#">&nbsp;编辑文章&nbsp;</a></li>
        </ul>
    </div>
    <div id="middle2">
        <form action="article_edit_save.php" method="post">
            <table border="1" style="font-size:14px; border-collapse:collapse;width:690px;height:400px; text-align:left;">
                <tr>
                    <td>文章id:</td>
                    <td><input type="text" name="article_id" VALUE="<?PHP echo $article['article_id']?>" readonly="readonly"/></td>
                </tr>
                <tr>
                    <td>新闻标题:</td>
                    <td><input type="text" name="article_title" value="<?PHP echo $article['article_title']?>"/></td>
                </tr>

                <tr>
                    <td>新闻分类:</td>
                    <td><input type="text" name="category_id" value="<?PHP echo $article['category_id']?>"/></td>
                </tr>
                <tr>
                    <td><td>新闻内容:</td>
                    <td><textarea rows="15" cols="60" name="intro"><?PHP echo $article['intro']?></textarea></td></td>
                </tr>
                <tr>
                    <td>文章简介:</td>
                    <td><textarea rows="15" cols="60" name="content"><?PHP echo $article['content']?></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="提交"/>
                        <input type="reset" value="重置" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>