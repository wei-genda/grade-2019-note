CREATE DATABASE IF NOT EXISTS blog
DEFAULT CHARACTER SET utf8mb4
DEFAULT COLLATE utf8mb4_general_ci;

use blog;

-- 分类表
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,-- 分类id：int型，自增，无符号，非空
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',-- 分类名称：varchar类型， 非空
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述', -- 分类描述：varchar类型 非空
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间', -- 更新时间：int型，非空，默认值为0
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',-- 增加时间：int型，非空
  PRIMARY KEY (`category_id`)-- 设置分类id为主键
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 comment='分类表';

-- 文章表
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,-- 文章id：int型，非空
  `category_id` int(11) NOT NULL COMMENT '所属分类',-- 分类：int型，非空
  `article_title` varchar(45) NOT NULL COMMENT '标题',-- 标题：varhchar型，非空
  `content` longtext NOT NULL COMMENT '文章内容',-- 文章内容：长文本，非空
  `update_time` int(11) NOT NULL COMMENT '更新时间',-- 更新时间：int型，非空
  `add_time` int(11) NOT NULL COMMENT '增加时间',-- 增加时间：int型，非空
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 comment='文章表';

show TABLES;