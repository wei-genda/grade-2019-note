-- 创建数据库OA
create database if not exists OA
	default character set utf8mb4
	default collate utf8mb4_general_ci;
	
use OA;	

-- 创建员工信息表employee，表引擎为innodb，表字符集为utf8mb4

create table employee(
	`id` INT(11) unsigned auto_increment not null comment '员工编号',
	`name` VARCHAR(25) not null comment '员工名称',
	`deptId` INT(11) unsigned not null default '0' comment '所在部门编号',
	`salary` FLOAT(11,2) unsigned not null  default '0.00' comment '工资',
	`update_time` INT(11) unsigned not null comment '修改时间',
	`add_time` INT(11) unsigned not null comment '增加时间',
	PRIMARY KEY (`id`)
)engine=innodb default charset=utf8mb4 comment='员工信息表';

-- 修改 employee 表名为 employee_info

alter table employee rename to employee_info;

-- 员工表增加字段

alter table employee_info add sex tinyint(3) unsigned default '3' comment '员工性别，1男2女3保密';
alter table employee_info add address VARCHAR(100) not null default '' comment '地址';
alter table employee_info add join_time INT(11) not null comment '入职时间';

-- 员工信息表工资salary字段进行修改

alter table employee_info change salary salary decimal(11,2) unsigned not null default '0' comment '工资';

-- 删除字段员工信息表update_time 

alter table employee_info drop update_time;


-- 创建部门信息表department，表引擎为innodb，表字符集为utf8mb4

create table department(
	`deptId` INT(11) unsigned auto_increment not null comment '部门编号',
	`name` VARCHAR(25) not null comment '部门名称',
	`level` INT(11) unsigned not null default '0' comment '部门等级',
	`parentDeptId` INT(11) unsigned not null default '0' comment '上级部门编号',
	`deptLeader` INT(11) unsigned not null default '0' comment '部门领导',
	`update_time` INT(11) unsigned not null comment '修改时间',
	`add_time` INT(11) unsigned not null comment '增加时间',
	PRIMARY KEY(`deptId`)
)engine=innodb default charset=utf8mb4 comment='部门信息表';

-- 修改 department 表名为 department_info

alter table department rename to department_info;