-- 创建电子商务数据库，eshop：
create database if not exists eshop
	default character set utf8mb4
	default collate utf8mb4_general_ci;
	
use eshop;

-- 创建商品表item：
create table `item` (
	`item_id` INT(11) unsigned not null auto_increment,
	`item_name` VARCHAR(100) not null comment '商品名称',
	`item_price` DECIMAL(10,2) not null comment '商品价格',
	PRIMARY KEY (`item_id`)
)engine=innodb default charset=utf8mb4 comment='商品表';

-- 创建购物车表cart:
create table `cart` (
	`cart_id` INT(11) unsigned not null auto_increment,
	`user_id` INT(11) unsigned not null comment '用户id',
	`item_id` INT(11) unsigned not null comment '商品id',
	`item_num` INT(11) unsigned not null default '1' comment '商品数量',
	`add_time` INT(11) unsigned not null default '0' comment '加入时间',
	PRIMARY KEY (`cart_id`)
)engine=innodb default charset=utf8mb4;

-- 插入数据

-- 商品表item：
INSERT INTO item ( item_name, item_price )
VALUES
	( '小米全面屏电视 43英寸 E43K 全高清 DTS-HD 震撼音效 1GB+8GB 智能网络液晶平板电视 L43M5-EK', 1349 ),
	( 'TCL 55L8 55英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机', 2199 ),
	( '海信 VIDAA 43V1F-R 43英寸 全高清 海信电视 全面屏电视 智慧屏 1G+8G 教育电视 人工智能液晶平板电视', 1349 ),
	( '海信（Hisense）60E3F 60英寸 4K超高清 智慧语音 超薄悬浮全面屏大屏精致圆角液晶电视机 教育电视 人工智能', 2699 ),
	( 'TCL 50L8 50英寸 4K超高清电视 智慧语音 超薄机身 杜比+DTS双解码 网络教育 智能液晶平板电视机', 1999 ),
	( '长虹 55D4P 55英寸超薄无边全面屏 4K超高清 手机投屏 智能网络 教育电视 平板液晶电视机（黑色）', 2099 );
	
-- 购物车cart：
INSERT INTO cart ( user_id, item_id, item_num, add_time )
VALUES
	(
		1,
		1,
		1,
		unix_timestamp(
		now())),
	(
		2,
		2,
		2,
		unix_timestamp(
		now())),
	(
		3,
		4,
		1,
		unix_timestamp(
		now())),
	(
		3,
		5,
		1,
		unix_timestamp(
		now())),
	(
		4,
		1,
		2,
		unix_timestamp(
		now()));
	
-- 查询数据
	
-- 1. 查询商品表所有数据。
select * from item;
-- 2. 查询价格小于2000的商品。
select * from item where item_price < 2000;
-- 3. 查询名称中包含 海信 的商品。
select * from item where item_name like '%海信%';
-- 4. 查询商品表，假设每页3条记录，查询第1页的数据。
select * from item limit 3;
-- 5. 查询商品表，假设每页3条记录，查询第2页的数据。
select * from item limit 3 offset 3;
-- 6. 查询所有商品，按照价格进行升序排序。
select * from item order by item_price asc;
-- 7. 查询价格小于2000的商品总数。
select count(*) from item where item_price < 2000;
-- 8. 查询购物车中所有数据。
select * from cart;
-- 9. 查询每个用户加入购物车的商品总数。
select user_id,sum(item_num) from cart group by user_id;
-- 10. 查询加入购物车的商品总数小于2的用户。
select user_id from cart group by user_id having sum(item_num) < 2 ;
-- 11. 查询有加入购物车的商品。
select distinct item.* from item inner join cart on cart.item_id=item.item_id;	-- 连接查询，DISTINCT表示去除重复的意思
select * from item where item_id in (select item_id from cart );   -- 子查询方式，不推荐，mysql性能比较低
-- 12. 查询没有加入购物车的商品。
select * from item where item_id not in (select item_id from cart);		-- 子查询
select item.* from item left join cart on item.item_id=cart.item_id where isnull (cart.cart_id); 	-- 连接查询

-- 修改数据

-- 1.修改商品表，商品id为1的价格为1249：
update item set item_price=1249 where item_id=1;
-- 2.用户3多加了一件商品5到购物车:
update cart set item_num=item_num+1 where user_id=3 and item_id=5;

-- 删除数据

-- 1. 用户4将将商品1从购物车中移除。
delete from item where user_id=4 and item_id=1;
-- 2. 用户3清空购物车。
delete from cart where user_id=3;
-- 3. 移除购物车中的所有数据：
-- 	1. 使用delete进行删除
	delete from cart;
-- 	2. 使用truncate进行删除
	truncate table cart;
-- 	3. 对比delete和truncate的差别
-- 解答：1.delete是逐行删除数据，而truncate是直接删除原来的表再建一个新表，执行数据比delete快。
			-- 2.delete删除数据后，配合事件回滚可以找回数据，truncate不支持事务的回滚，数据删除后无法找回。
			-- 3.delete可以通过where条件指定条件删除部分数据，truncate不支持where子句，只能删除整体。
			-- 4.delete删除后自增列无法重置（无法从1再次开始），而truncate自增列可以重置（从1开始）。
			-- 5.delete返回删除数据的行数，truncate只返回0，没有任何意义。