<?php
/**
 * 文章增加
 */
date_default_timezone_set("PRC");

// 连接数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$sql = "select * from category order by category_id desc";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="css/article.css" type="text/css"/>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<h1>博客管理系统</h1>
				<div id="admin-info">欢迎你：admin <a href="#">退出登录</a></div>
			</div>
			<div id="left">
				<ul>
					<li><a href="category.php">分类管理</a></li>
					<li><a href="article.php">文章管理</a></li>
					<li><a href="#">管理员</a></li>
				</ul>
			</div>
			<div id="right">
				<div id="right-content">
					<div id="table-info">
						<a href="article.php">首页</a>&gt;
						<a href="article.php">文章管理</a>&gt;
						<a href="article_add.php">增加文章</a>
					</div>
					<div id="table-list">
                        <form action="article_add_save.php" method="post">
                            <table width="100%" border="1px solid" cellspacing="0px" cellpadding="0">
                                <tr>
                                    <td>文章标题：</td>
                                    <td><input type="text" name="article_title"/></td>
                                </tr>
                                <tr>
                                    <td>所属分类：</td>
                                    <td>
                                        <select name="category_id" >
                                            <?php foreach ($categoryList as $row): ?>
                                                <option value="<?php echo $row['category_id']; ?>">
                                                    <?php echo $row['category_name']?>
                                                </option>
                                            <?php endforeach;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>文章简介：</td>
                                    <td><input type="text" name="intro"/></td>
                                </tr>
                                <tr>
                                    <td>文章内容：</td>
                                    <td><textarea name="content" style="width: 400px; height: 200px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="submit" class="btn"/>
                                        <input type="reset" class="btn"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
