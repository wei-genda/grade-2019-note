<?php
/**
 * 分类编辑
 */
$categoryId = $_GET['category_id'];

// 保存到数据库
date_default_timezone_set("PRC");

$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$sql = "select * from category where category_id = '$categoryId'";
$result = $db->query($sql);
$category = $result->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="css/article.css" type="text/css"/>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<h1>博客管理系统</h1>
				<div id="admin-info">欢迎你：admin <a href="#">退出登录</a></div>
			</div>
			<div id="left">
				<ul>
					<li><a href="category.php">分类管理</a></li>
					<li><a href="article">文章管理</a></li>
					<li><a href="#">管理员</a></li>
				</ul>
			</div>
			<div id="right">
				<div id="right-content">
					<div id="table-info">
						<a href="article.php">首页</a>&gt;
						<a href="category.php">分类管理</a>&gt;
						<a href="category_edit.php">编辑分类</a>
					</div>
					<div id="table-list">
                        <form action="category_edit_save.php" method="post">
                            <table width="100%" border="1px solid" cellspacing="0px" cellpadding="0">
                                <tr>
                                    <td width="190">分类Id：</td>
                                    <td><input type="text" value="<?php echo $category['category_id']; ?>" readonly="readonly" name="category_id"/></td>
                                </tr>
                                <tr>
                                    <td>分类名称：</td>
                                    <td><input type="text" name="category_name" value="<?php echo $category['category_name'] ?>"/></td>
                                </tr>
                                <tr>
                                    <td>分类描述：</td>
                                    <td><textarea name="category_desc" style="width: 400px; height: 200px;" ><?php echo $category['category_desc']; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="submit" class="btn"/>
                                        <input type="reset" class="btn"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
