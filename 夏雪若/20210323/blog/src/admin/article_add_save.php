<?php
/**
 *  文章增加保存功能
 */
//var_dump($_POST);
//exit();
// 获取到文章数据
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$intro = $_POST['intro'];
$content = $_POST['content'];

if (mb_strlen($articleTitle) < 5 || mb_strlen($articleTitle) > 50){
    echo '文章标题限制5~50个字';
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}


// 保存到数据库
date_default_timezone_set("PRC");

$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

$sql = "insert into article(article_title,category_id,intro,content,update_time,add_time) 
  values ('$articleTitle','$categoryId','$intro','$content','$updateTime','$addTime')";
$result = $db->exec($sql);
if ($result){
    echo "插入成功.<a href='article.php'>返回列表页</a>";
    exit();
}else{
    echo "插入失败，错误信息：" . $db->errorInfo()[2].",请联系管理员：2103918233@qq.com";
}