<?php
/**
 * 分类增加
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" href="css/article.css" type="text/css"/>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<h1>博客管理系统</h1>
				<div id="admin-info">欢迎你：admin <a href="#">退出登录</a></div>
			</div>
			<div id="left">
				<ul>
					<li><a href="category.php">分类管理</a></li>
					<li><a href="article">文章管理</a></li>
					<li><a href="#">管理员</a></li>
				</ul>
			</div>
			<div id="right">
				<div id="right-content">
					<div id="table-info">
						<a href="article.php">首页</a>&gt;
						<a href="category.php">分类管理</a>&gt;
						<a href="category_add.php">增加分类</a>
					</div>
					<div id="table-list">
                        <form action="category_add_save.php" method="post">
                            <table width="100%" border="1px solid" cellspacing="0px" cellpadding="0">
                                <tr>
                                    <td>分类名称：</td>
                                    <td><input type="text" name="category_name"/></td>
                                </tr>
                                <tr>
                                    <td>分类描述：</td>
                                    <td><textarea name="category_desc" style="width: 400px; height: 200px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="submit" class="btn"/>
                                        <input type="reset" class="btn"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
