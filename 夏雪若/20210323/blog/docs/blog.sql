/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-24 10:43:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '标题',
  `intro` varchar(255) DEFAULT NULL COMMENT '简介',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '2', 'css入门', '基础语法及操作', '基础语法及操作', '1616545259', '1616545259');
INSERT INTO `article` VALUES ('2', '1', 'html入门', '基础语法及操作', '基础语法及操作。', '1616546990', '1616545292');
INSERT INTO `article` VALUES ('3', '3', 'java入门', '基础语法及操作', '', '1616547407', '1616547407');
INSERT INTO `article` VALUES ('4', '4', 'php入门', '基础语法及操作', 'php基础语法及操作', '1616547453', '1616547453');
INSERT INTO `article` VALUES ('5', '5', 'mysql入门', '基础语法及操作', 'mysql基础语法及操作', '1616550927', '1616547495');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'html', 'html基础知识', '1616544892', '1616544883');
INSERT INTO `category` VALUES ('2', 'css', 'css基础知识', '1616544912', '1616544912');
INSERT INTO `category` VALUES ('3', 'java', 'java基础知识', '1616544937', '1616544937');
INSERT INTO `category` VALUES ('4', 'php', 'php基础知识', '1616544958', '1616544958');
INSERT INTO `category` VALUES ('5', 'mysql', 'mysql基础知识', '1616545001', '1616545001');
