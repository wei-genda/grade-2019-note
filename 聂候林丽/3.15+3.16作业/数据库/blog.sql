/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-17 08:31:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for artilce_copy
-- ----------------------------
DROP TABLE IF EXISTS `artilce_copy`;
CREATE TABLE `artilce_copy` (
  `artilce_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `artilce_title` varchar(100) NOT NULL COMMENT '文章标题',
  `categord_id` int(11) unsigned NOT NULL COMMENT '所属分类',
  `artilce_author` varchar(20) DEFAULT NULL COMMENT '作者',
  `artilce_content` text COMMENT '文章内容',
  `update_time` int(11) unsigned NOT NULL COMMENT '修改时间',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '增加时间',
  PRIMARY KEY (`artilce_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of artilce_copy
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ld',
  `category_name` varchar(20) NOT NULL COMMENT '分类名称',
  `category_daes` varchar(1000) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of category
-- ----------------------------
