-- 创建分类表
CREATE TABLE `category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_name` varchar(20) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(1000) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 创建文章表
CREATE TABLE `article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `article_title` varchar(100) NOT NULL COMMENT '文章标题',
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_author` varchar(20) DEFAULT NULL COMMENT '作者',
  `article_content` text COMMENT '文章内容',
  `update_time` int(11) unsigned NOT NULL COMMENT '修改时间',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;