<?php
/**
*分类管理首页
 */
$dsn = "mysql:Server=127.0.0.1;Database=blog;";
$db = new PDO($dsn, "root", "123456");

$sql = "SELECT * FROM category order by category_id desc";
$statement = $db->query($sql);
$taskList = $statement->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/base_category.css" type="text/css">
    <title>无标题文档</title>
</head>

<body><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
</head>
<body>
<div id="container">
    <div id="top">

        <h1>博客系统</h1>
        <div id="login_info">
            欢迎你：admin
            <a href="logout.html">退出登录</a>
        </div>
    </div>

    <div id="left">

        <li><a href="#">分类管理</a></li>
        <li><a href="#">新闻管理</a></li>
        <li><a href="#">管理员</a></li>
    </div>

    <div id="right">

        <div id="banner">
            <a href="#">首页</a>&gt;
            <a href="#">新闻管理</a>&gt;
            <a href="#">新闻列表</a>&gt;
        </div>
        <div id="choose_div">
            <button id="all">全选</button>
            <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>
            <a id="add" href="update.html" style="float: right">增加任务</a>
        </div>
        <div id="table_content">
            <table class="list">
                <div id="table_content">
                    <tbody>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>标题</th>
                        <th>简介</th>
                        <th>分类</th>
                        <th>发表时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <tr>
                        <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value=""></td>
                        <td>1</td>
                        <td>vue新手入门指南</td>
                        <td>web前端</td>
                        <td>vue新手入门指南，适合新手用户</td>
                        <td>2021-01-03 20:21:00</td>
                        <td>2021-01-03 20:21:00</td>
                        <td>
                            <a class="update_ing" href="detail.html">详情</a>
                        </td>
                    </tr>
            </table>
            </tbody>
        </div>
    </div>
</div>

</body>
</html>

</body>
</html>

