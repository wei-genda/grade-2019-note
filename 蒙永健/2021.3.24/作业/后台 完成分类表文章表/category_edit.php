<?php
/**
详情页
 *
 */
$categoryId=$_GET['id'];
$dsn = "mysql:Server=127.0.0.1;dbname=blog;";
$db = new PDO($dsn, "root", "123456");

$sql = "SELECT * FROM category where category_id = '$categoryId'";
$statement = $db->query($sql);
$categoryList = $statement->fetch(PDO::FETCH_ASSOC);
date_default_timezone_set("prc");
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  rel="stylesheet" href="css/base_increase.css" type="text/css"/>
    <title>无标题文档</title>
</head>

<body><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
</head>
<body>
<div id="container">
    <div id="top">

        <h1>博客系统</h1>
        <div id="login_info">
            欢迎你：admin
            <a href="logout.html">退出登录</a>
        </div>
    </div>

    <div id="left">

        <li><a href="classify_list.php">分类管理</a></li>
        <li><a href="article_list.php">文章管理</a></li>
        <li><a href="#">管理员</a></li>
    </div>

    <div id="right">

        <div id="banner">
            <a href="#">首页</a>&gt;
            <a href="#">分类管理</a>&gt;
            <a href="#">分类详情</a>&gt;
        </div>
        <div id="text_add">

            <form action="category_edit_save.php" method="post">
                <table class="update">
                    <tr>
                        <td>新闻id：</td>
                        <td>
                            <input name="category_id" type="text"  value="<?php echo $categoryList['category_id']?>" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td>新闻标题：</td>
                        <td><input name="category_name" type="text" value="<?php echo $categoryList['category_name']?>"  /></td>
                    </tr>

                    <tr>
                        <td>分类简介：</td>
                        <td><textarea name="category_desc" cols="60" rows="15"><?php echo $categoryList['category_desc']?></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" value="提交" class="btn" />
                            <input type="reset" value="重置" class="btn" />
                        </td>
                    </tr>
                </table>
            </form>

        </div>
    </div>
</div>

</body>
</html>

</body>
</html>
